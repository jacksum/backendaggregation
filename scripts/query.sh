#! /usr/bin/env expect

set timeout -1

set sql_path [lindex $argv 0]
set sql_pass [lindex $argv 1]

if {$sql_pass eq ""} {set sql_pass "voc"}

spawn mclient -u voc -d voc $sql_path
expect "password:"
send -- "$sql_pass\r"

expect eof
exit