#! /usr/bin/env expect

set timeout -1

set script_path $argv0
set dirname [exec dirname $script_path]
set projec_path [exec greadlink -f "$dirname/.."]
set dbfarm_path "$projec_path/dbfarm"
set user_sql_path "$dirname/createUser.sql"
set dump_sql_path "$dirname/voc_dump.sql"

spawn monetdbd create $dbfarm_path
expect {
  -re "already initialised" {
    exec monetdbd stop $dbfarm_path
    exec rm -rf $dbfarm_path
    exec monetdbd create $dbfarm_path
    exp_continue
  }
}

spawn monetdbd start $dbfarm_path
expect {
  -re "Address already in use" {
    exec killall -9 monetdbd
    exec monetdbd start $dbfarm_path
    exp_continue
  }
}

exec monetdb create voc
exec monetdb release voc

spawn mclient -u monetdb -d voc $user_sql_path
expect "password:"
send -- "monetdb\r"

send_user -- "\r"
exec sleep 2

spawn mclient -u voc -d voc < $dump_sql_path
expect "password:"
send -- "voc\r"

expect eof
exit