--explain
-- SELECT
--   sys.str_to_date(sys.date_to_str(date '2020-03-31', '%Y-%m'), '%Y-%m') + INTERVAL '3' MONTH AS "date";

-- SELECT sys.str_to_date(sys.date_to_str(date '2019-03-31', '%Y-%m'), '%Y-%m')  AS "date";
-- SELECT sys.str_to_date(sys.concat(sys.date_to_str(date '2019-03-31', '%Y-%m'), '-01'), '%Y-%m-%d') AS "date";

-- SELECT
--   sys.str_to_date(sys.date_to_str(departure_date, '%Y-%m'), '%Y-%m') + INTERVAL '3' MONTH AS "date"
-- FROM
-- voyages
-- LIMIT 3;


SELECT
  departure_date + INTERVAL '3' MONTH AS "date_offset"
FROM
  voyages
WHERE
  EXTRACT(YEAR from departure_date) = 1602
  AND
  EXTRACT(MONTH from departure_date) = 3;

SELECT
  sys.str_to_date(sys.concat(sys.date_to_str(departure_date, '%Y-%m'), '-01'), '%Y-%m-%d') + INTERVAL '3' MONTH AS "date_offset"
FROM
  voyages
WHERE
  EXTRACT(YEAR from departure_date) = 1602
  AND
  EXTRACT(MONTH from departure_date) = 3;