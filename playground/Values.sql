WITH
  t AS (
    SELECT
      date_year
    FROM
      (VALUES
        (2000),
        (2001),
        (2002)
      ) v(date_year)
  ),
  t2 AS (
    SELECT
      date_year,
      sales
    FROM
      (VALUES
        (2000, 100),
        (2000, 200),
        (2001, 300),
        (2001, 400),
        (2002, 500),
        (2002, 600)
      ) v(date_year, sales)
  )
SELECT
  *
FROM
  t INNER JOIN t2 ON t.date_year >= t2.date_year;