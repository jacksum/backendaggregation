CREATE FUNCTION test_func ()
RETURNS INT
BEGIN
  DECLARE x INT;
  SELECT
    COUNT(*)
  INTO
    x
  FROM
    voyages;
  RETURN x;
END;

-- EXPLAIN
SELECT test_func();