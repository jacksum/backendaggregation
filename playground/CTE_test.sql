-- Note: Recursive queries are NOT supported yet

WITH DirectdReports AS
(  
    SELECT ManagerID, EmployeeID, Title, 0 AS EmployeeLevel  
    FROM cte_test   
    WHERE ManagerID IS NULL  
    UNION ALL  
    SELECT e.ManagerID, e.EmployeeID, e.Title, EmployeeLevel + 1  
    FROM cte_test AS e  
        INNER JOIN DirectReports AS d  
        ON e.ManagerID = d.EmployeeID   
)  
SELECT ManagerID, EmployeeID, Title, EmployeeLevel   
FROM DirectReports  
ORDER BY ManagerID;   