{
  dimensions: [
    {
      columnId: 'type_of_boat',
      subColumnType: 0
    },
    {
      columnId: 'arrival_date',
      subColumnType: 1
    },
  ],
  values: [
    {
      aggregationType: 'avg',
      column: {
        columnId: 'tonnage',
        subColumnType: 0
      }
    },
    {
      value: {
        aggregationType: 'avg',
        column: {
          columnId: 'tonnage',
          subColumnType: 0
        }
      },
      calculateBy: {
        type: 'accumulation',
        partitionBy: [{
          columnId: 'type_of_boat',
          subColumnType: 1
        }]
      },
      options: {
        accumulationType: 'avg',
      }
    },
  ],
};
-- EXPLAIN
WITH
  raw_table AS (
    SELECT
      tonnage,
      EXTRACT(YEAR FROM arrival_date) AS arrival_date_year,
      type_of_boat
    FROM
      voyages
    WHERE
      EXTRACT(YEAR FROM arrival_date) > 1700
      AND
      EXTRACT(YEAR FROM arrival_date) < 1705
      AND
      type_of_boat IN ('fluit', 'pinas')
      AND
      tonnage IS NOT NULL
  ),
  raw_table_with_running_order AS (
    SELECT
      *,
      DENSE_RANK() OVER (
        PARTITION BY
          type_of_boat
        ORDER BY
          arrival_date_year ASC        
      ) AS running_order
    FROM
      raw_table
  ),
  grouped_table_with_running_order AS (
    SELECT
      AVG(tonnage) AS avg_of_tonnage,
      arrival_date_year,
      type_of_boat,
      DENSE_RANK() OVER (
        PARTITION BY
          type_of_boat
        ORDER BY
          arrival_date_year ASC        
      ) AS running_order
    FROM
      raw_table
    GROUP BY
      arrival_date_year,
      type_of_boat
  ),
  table_with_running AS (
    SELECT
      g.arrival_date_year,
      g.type_of_boat,
      g.avg_of_tonnage,
      r.tonnage AS running_tonnage
    FROM
      raw_table_with_running_order AS r
      INNER JOIN
      grouped_table_with_running_order AS g
      ON (
        r.running_order <= g.running_order
        AND
        r.type_of_boat = g.type_of_boat
      )
  )
-- SELECT
--   *
-- FROM
--   raw_table_with_running_order
-- ORDER BY
--   arrival_date_year ASC,
--   type_of_boat ASC;

SELECT
  type_of_boat,
  arrival_date_year,
  MAX(avg_of_tonnage) AS avg_of_tonnage,
  AVG(running_tonnage) AS running_sum_of_tonnage
FROM
  table_with_running
GROUP BY
  arrival_date_year,
  type_of_boat
ORDER BY
  arrival_date_year ASC,
  type_of_boat ASC;
