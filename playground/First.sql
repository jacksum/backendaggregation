SELECT
  DISTINCT master,
  sys.first_value(type_of_boat) OVER(
    PARTITION BY
      master
  )
FROM
  voyages
WHERE
  type_of_boat IS NOT NULL
ORDER BY
  master ASC;
