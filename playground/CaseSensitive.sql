-- SELECT
--   *
-- FROM
--   voyages
-- WHERE master ILIKE '%stok%'
-- LIMIT 10;


-- SELECT
--   *
-- FROM
--   voyages
-- WHERE LCASE(master) = LCASE('stokmans')
-- LIMIT 10;

EXPLAIN
SELECT
  *
FROM
  voyages
WHERE LCASE(master) IN (LCASE('stokmans'))
LIMIT 10;