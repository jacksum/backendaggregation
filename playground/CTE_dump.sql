CREATE TABLE cte_test (  
  EmployeeID SMALLINT NOT NULL,  
  FirstName VARCHAR(30)  NOT NULL,  
  LastName  VARCHAR(40) NOT NULL,  
  Title VARCHAR(50) NOT NULL,  
  DeptID SMALLINT NOT NULL,  
  ManagerID INT
);

INSERT INTO cte_test VALUES   
 (1, 'Ken', 'Sánchez', 'Chief Executive Officer',16,NULL)  
,(273, 'Brian', 'Welcker', 'Vice President of Sales',3,1)  
,(274, 'Stephen', 'Jiang', 'North American Sales Manager',3,273)  
,(275, 'Michael', 'Blythe', 'Sales Representative',3,274)  
,(276, 'Linda', 'Mitchell', 'Sales Representative',3,274)  
,(285, 'Syed', 'Abbas', 'Pacific Sales Manager',3,273)  
,(286, 'Lynn', 'Tsoflias', 'Sales Representative',3,285)  
,(16,  'David','Bradley', 'Marketing Manager', 4, 273)  
,(23,  'Mary', 'Gibson', 'Marketing Specialist', 4, 16);  