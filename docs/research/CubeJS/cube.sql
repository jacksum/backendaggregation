-- https://github.com/cube-js/cube.js

SELECT
   q_0.\"orders__created_at_month\",
   \"orders__moving_number\" \"orders__moving_number\" 
FROM
   (
      SELECT
         \"Orders.createdAt_series\".\"date_from\" \"orders__created_at_month\",
         sum(\"orders__moving_number\") \"orders__moving_number\" 
      FROM
         (
            SELECT
               date_from::timestamp,
               date_to::timestamp 
            FROM
               (
               VALUES
                  (
                     '2019-01-01T00:00:00.000',
                     '2019-01-31T23:59:59.999'
                  )
,
                  (
                     '2019-02-01T00:00:00.000',
                     '2019-02-28T23:59:59.999'
                  )
,
                  (
                     '2019-03-01T00:00:00.000',
                     '2019-03-31T23:59:59.999'
                  )
,
                  (
                     '2019-04-01T00:00:00.000',
                     '2019-04-30T23:59:59.999'
                  )
,
                  (
                     '2019-05-01T00:00:00.000',
                     '2019-05-31T23:59:59.999'
                  )
,
                  (
                     '2019-06-01T00:00:00.000',
                     '2019-06-30T23:59:59.999'
                  )
,
                  (
                     '2019-07-01T00:00:00.000',
                     '2019-07-31T23:59:59.999'
                  )
,
                  (
                     '2019-08-01T00:00:00.000',
                     '2019-08-31T23:59:59.999'
                  )
,
                  (
                     '2019-09-01T00:00:00.000',
                     '2019-09-30T23:59:59.999'
                  )
,
                  (
                     '2019-10-01T00:00:00.000',
                     '2019-10-31T23:59:59.999'
                  )
,
                  (
                     '2019-11-01T00:00:00.000',
                     '2019-11-30T23:59:59.999'
                  )
,
                  (
                     '2019-12-01T00:00:00.000',
                     '2019-12-31T23:59:59.999'
                  )
               )
               AS dates (date_from, date_to)
         )
         AS \"Orders.createdAt_series\" 
         LEFT JOIN
            (
               SELECT
                  date_trunc('month', (\"orders_moving_number_cumulative___orders\".created_at::timestamptz AT TIME ZONE 'UTC')) \"orders__created_at_month\",
                  sum(\"orders_moving_number_cumulative___orders\".number) \"orders__moving_number\" 
               FROM
                  public.orders AS \"orders_moving_number_cumulative___orders\" 
               WHERE
                  (
                     (\"orders_moving_number_cumulative___orders\".created_at::timestamptz AT TIME ZONE 'UTC') > $1::timestamp - interval '2 month' 
                     AND 
                     (\"orders_moving_number_cumulative___orders\".created_at::timestamptz AT TIME ZONE 'UTC') <= $2::timestamp
                  )
               GROUP BY
                  1
            ) AS \"orders_moving_number_cumulative__base\" 
            ON \"orders_moving_number_cumulative__base\".\"orders__created_at_month\" > \"Orders.createdAt_series\".\"date_to\" - interval '2 month' 
            AND \"orders_moving_number_cumulative__base\".\"orders__created_at_month\" <= \"Orders.createdAt_series\".\"date_to\" 
      GROUP BY
         1
   )
   as q_0 
ORDER BY
   1 ASC LIMIT 10000