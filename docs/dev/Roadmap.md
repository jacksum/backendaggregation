### Future Work
* Frontend
    * Dev Checklist
        1. Scenario Model: adapt to new aggregation API
        1. DataView Builder: adapt to new aggregation structure
        1. AggregationConfig Builder: rewrite to fit the new aggregation API
        1. Resource Model: adapt to new aggregation API
        1. Some data structures may be adjusted: rank, filter, calculation, ...
        1. PivotTable: How to reshape it within new aggregation model without functionality lost?
            * Custom Data
            * ...
        1. DataTable: unchanged?
        1. Slicer: unchanged?
    * Test Checklist
        1. All aggregation type (sum, count, ...)
        1. All calculations (running, moving, ...)
        1. All filters
        1. All ranks
        1. All sorts
        1. Multi-dataset scenario (richText, calcChart)
        1. template scenario (map, kpiMatrix)
        1. tables (dataTable, pivotTable)
        1. Others
            1. MoM/YoY in KPI/Gauge
            1. Custom data
            1. Slicer/plugin slicer
            1. cross filter/drill down
* Backend
    1. SQL Builder: translate pivot model to SQL
    1. Migration