### Fit PivotTable to new model

1. PivotSetting State
```ts
{
  values: {
    values: [
      {
        name: 'price',
        valueType: 1,
        method: 'sum',
        idx: 0
      }
    ]
  },
  rows: {
    values: [
      {
        name: 'department',
        valueType: 1
      },
      {
        name: 'employee',
        valueType: 1
      }
    ]
  },
  columns: {
    values: [
      {
        name: 'date\u001d(Year)',
        valueType: 1
      },
      {
        name: 'date\u001d(Quarter)',
        valueType: 1
      }
    ]
  }
}
```

1. Aggregation Model  
Generate 9 pivot models with same id for this pivot table aggregation.
```ts
{
  pivots: [
    {
      id: '1',
      values: [{
        aggregationType: 'sum',
        column: {
          columnId: 'price',
          subColumnType: 0,
        }
      }],
      dimensions: [ // 2 col - 2 row
        {
          column: {
            columnId: 'department',
            subColumnType: 0,
          }
        },
        {
          column: {
            columnId: 'employee',
            subColumnType: 0,
          }
        },
        {
          column: {
            columnId: 'date',
            subColumnType: 1,
          }
        },
        {
          column: {
            columnId: 'date',
            subColumnType: 2,
          }
        },
      ],
      filters: [],
    },
    {
      id: '1',
      values: [{
        aggregationType: 'sum',
        column: {
          columnId: 'price',
          subColumnType: 0,
        }
      }],
      dimensions: [ // 1 col - 2 row
        {
          column: {
            columnId: 'department',
            subColumnType: 0,
          }
        },
        {
          column: {
            columnId: 'employee',
            subColumnType: 0,
          }
        },
        {
          column: {
            columnId: 'date',
            subColumnType: 1,
          }
        },
      ],
      filters: [],
    },
    {
      id: '1',
      values: [{
        aggregationType: 'sum',
        column: {
          columnId: 'price',
          subColumnType: 0,
        }
      }],
      dimensions: [ // 0 col - 2 row
        {
          column: {
            columnId: 'department',
            subColumnType: 0,
          }
        },
        {
          column: {
            columnId: 'employee',
            subColumnType: 0,
          }
        },
      ],
      filters: [],
    },
    {
      id: '1',
      values: [{
        aggregationType: 'sum',
        column: {
          columnId: 'price',
          subColumnType: 0,
        }
      }],
      dimensions: [ // 2 col - 1 row
        {
          column: {
            columnId: 'department',
            subColumnType: 0,
          }
        },
        {
          column: {
            columnId: 'date',
            subColumnType: 1,
          }
        },
        {
          column: {
            columnId: 'date',
            subColumnType: 2,
          }
        },
      ],
      filters: [],
    },
    {
      id: '1',
      values: [{
        aggregationType: 'sum',
        column: {
          columnId: 'price',
          subColumnType: 0,
        }
      }],
      dimensions: [ // 2 col - 0 row
        {
          column: {
            columnId: 'date',
            subColumnType: 1,
          }
        },
        {
          column: {
            columnId: 'date',
            subColumnType: 2,
          }
        },
      ],
      filters: [],
    },
    {
      id: '1',
      values: [{
        aggregationType: 'sum',
        column: {
          columnId: 'price',
          subColumnType: 0,
        }
      }],
      dimensions: [ // 1 col - 1 row
        {
          column: {
            columnId: 'department',
            subColumnType: 0,
          }
        },
        {
          column: {
            columnId: 'date',
            subColumnType: 1,
          }
        },
      ],
      filters: [],
    },
    {
      id: '1',
      values: [{
        aggregationType: 'sum',
        column: {
          columnId: 'price',
          subColumnType: 0,
        }
      }],
      dimensions: [ // 1 col - 0 row
        {
          column: {
            columnId: 'date',
            subColumnType: 1,
          }
        },
      ],
      filters: [],
    },
    {
      id: '1',
      values: [{
        aggregationType: 'sum',
        column: {
          columnId: 'price',
          subColumnType: 0,
        }
      }],
      dimensions: [ // 0 col - 1 row
        {
          column: {
            columnId: 'department',
            subColumnType: 0,
          }
        },
      ],
      filters: [],
    },
    {
      id: '1',
      values: [{
        aggregationType: 'sum',
        column: {
          columnId: 'price',
          subColumnType: 0,
        }
      }],
      dimensions: [], // 0 col - 0 row
      filters: [],
    },
  ],
  tables: []
  filters: []
}
```

1. Matrix View  
Concatenate values from all aggregated results into single list.
```json
{
  "columns": {
    "levels": [
      "date(Year)",
      "date(Quarter)"
    ],
    "headers": [
      {
        "column": "date(Year)",
        "label": 2014,
        "next": [
          {
            "column": "date(Quarter)",
            "label": 1,
            "next": []
          },
          {
            "column": "date(Quarter)",
            "label": 2,
            "next": []
          },
          {
            "column": "date(Quarter)",
            "label": 3,
            "next": []
          },
          {
            "column": "date(Quarter)",
            "label": 4,
            "next": []
          }
        ]
      }
    ],
    "sortPriority": -2
  },
  "rows": {
    "levels": [
      "department",
      "employee"
    ],
    "headers": [
      {
        "column": "department",
        "label": "DTDx",
        "next": [
          {
            "column": "employee",
            "label": "Paul",
            "next": []
          },
          {
            "column": "employee",
            "label": "Willi",
            "next": []
          },
          {
            "column": "employee",
            "label": "Winking",
            "next": []
          },
          {
            "column": "employee",
            "label": "Iver",
            "next": []
          }
        ]
      },
      {
        "column": "department",
        "label": "DTD1",
        "next": [
          {
            "column": "employee",
            "label": "Paul",
            "next": []
          },
          {
            "column": "employee",
            "label": "Willi",
            "next": []
          },
          {
            "column": "employee",
            "label": "Winking",
            "next": []
          },
          {
            "column": "employee",
            "label": "Iver",
            "next": []
          }
        ]
      },
      {
        "column": "department",
        "label": "DTD2",
        "next": [
          {
            "column": "employee",
            "label": "Paul",
            "next": []
          },
          {
            "column": "employee",
            "label": "Willi",
            "next": []
          },
          {
            "column": "employee",
            "label": "Winking",
            "next": []
          },
          {
            "column": "employee",
            "label": "Iver",
            "next": []
          }
        ]
      },
      {
        "column": "department",
        "label": "DTD3",
        "next": [
          {
            "column": "employee",
            "label": "Paul",
            "next": []
          },
          {
            "column": "employee",
            "label": "Willi",
            "next": []
          },
          {
            "column": "employee",
            "label": "Winking",
            "next": []
          },
          {
            "column": "employee",
            "label": "Iver",
            "next": []
          }
        ]
      }
    ],
    "sortPriority": -2
  },
  "values": [
    {
      "department": "DTDx",
      "employee": "Paul",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 8046
    },
    {
      "department": "DTDx",
      "employee": "Paul",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 4316
    },
    {
      "department": "DTDx",
      "employee": "Paul",
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 12169
    },
    {
      "department": "DTDx",
      "employee": "Paul",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 9504
    },
    {
      "department": "DTDx",
      "employee": "Willi",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 3990
    },
    {
      "department": "DTDx",
      "employee": "Willi",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 10373
    },
    {
      "department": "DTDx",
      "employee": "Willi",
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 3290
    },
    {
      "department": "DTDx",
      "employee": "Willi",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 10041
    },
    {
      "department": "DTDx",
      "employee": "Winking",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 2590
    },
    {
      "department": "DTDx",
      "employee": "Winking",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 4884
    },
    {
      "department": "DTDx",
      "employee": "Winking",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 6284
    },
    {
      "department": "DTDx",
      "employee": "Iver",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 4889
    },
    {
      "department": "DTDx",
      "employee": "Iver",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 7487
    },
    {
      "department": "DTDx",
      "employee": "Iver",
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 7280
    },
    {
      "department": "DTDx",
      "employee": "Iver",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 4593
    },
    {
      "department": "DTD1",
      "employee": "Paul",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 31193
    },
    {
      "department": "DTD1",
      "employee": "Paul",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 35930
    },
    {
      "department": "DTD1",
      "employee": "Paul",
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 69236
    },
    {
      "department": "DTD1",
      "employee": "Paul",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 48672
    },
    {
      "department": "DTD1",
      "employee": "Willi",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 31257
    },
    {
      "department": "DTD1",
      "employee": "Willi",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 46197
    },
    {
      "department": "DTD1",
      "employee": "Willi",
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 31763
    },
    {
      "department": "DTD1",
      "employee": "Willi",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 38186
    },
    {
      "department": "DTD1",
      "employee": "Winking",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 6884
    },
    {
      "department": "DTD1",
      "employee": "Winking",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 34893
    },
    {
      "department": "DTD1",
      "employee": "Winking",
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 14274
    },
    {
      "department": "DTD1",
      "employee": "Winking",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 15790
    },
    {
      "department": "DTD1",
      "employee": "Iver",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 20055
    },
    {
      "department": "DTD1",
      "employee": "Iver",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 28253
    },
    {
      "department": "DTD1",
      "employee": "Iver",
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 31188
    },
    {
      "department": "DTD1",
      "employee": "Iver",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 23180
    },
    {
      "department": "DTD2",
      "employee": "Paul",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 39008
    },
    {
      "department": "DTD2",
      "employee": "Paul",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 57636
    },
    {
      "department": "DTD2",
      "employee": "Paul",
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 78825
    },
    {
      "department": "DTD2",
      "employee": "Paul",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 33465
    },
    {
      "department": "DTD2",
      "employee": "Willi",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 29467
    },
    {
      "department": "DTD2",
      "employee": "Willi",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 78030
    },
    {
      "department": "DTD2",
      "employee": "Willi",
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 35276
    },
    {
      "department": "DTD2",
      "employee": "Willi",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 35587
    },
    {
      "department": "DTD2",
      "employee": "Winking",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 17625
    },
    {
      "department": "DTD2",
      "employee": "Winking",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 20705
    },
    {
      "department": "DTD2",
      "employee": "Winking",
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 36839
    },
    {
      "department": "DTD2",
      "employee": "Winking",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 21111
    },
    {
      "department": "DTD2",
      "employee": "Iver",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 22553
    },
    {
      "department": "DTD2",
      "employee": "Iver",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 36044
    },
    {
      "department": "DTD2",
      "employee": "Iver",
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 30992
    },
    {
      "department": "DTD2",
      "employee": "Iver",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 26635
    },
    {
      "department": "DTD3",
      "employee": "Paul",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 14765
    },
    {
      "department": "DTD3",
      "employee": "Paul",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 29451
    },
    {
      "department": "DTD3",
      "employee": "Paul",
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 32928
    },
    {
      "department": "DTD3",
      "employee": "Paul",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 14346
    },
    {
      "department": "DTD3",
      "employee": "Willi",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 11136
    },
    {
      "department": "DTD3",
      "employee": "Willi",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 27534
    },
    {
      "department": "DTD3",
      "employee": "Willi",
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 16988
    },
    {
      "department": "DTD3",
      "employee": "Willi",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 7194
    },
    {
      "department": "DTD3",
      "employee": "Winking",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 7883
    },
    {
      "department": "DTD3",
      "employee": "Winking",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 5888
    },
    {
      "department": "DTD3",
      "employee": "Winking",
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 13627
    },
    {
      "department": "DTD3",
      "employee": "Winking",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 9870
    },
    {
      "department": "DTD3",
      "employee": "Iver",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 16257
    },
    {
      "department": "DTD3",
      "employee": "Iver",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 19520
    },
    {
      "department": "DTD3",
      "employee": "Iver",
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 5052
    },
    {
      "department": "DTD3",
      "employee": "Iver",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 10500
    },
    {
      "department": "DTDx",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 19515
    },
    {
      "department": "DTDx",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 27060
    },
    {
      "department": "DTDx",
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 22739
    },
    {
      "department": "DTDx",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 30422
    },
    {
      "department": "DTD1",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 89389
    },
    {
      "department": "DTD1",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 145273
    },
    {
      "department": "DTD1",
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 146461
    },
    {
      "department": "DTD1",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 125828
    },
    {
      "department": "DTD2",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 108653
    },
    {
      "department": "DTD2",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 192415
    },
    {
      "department": "DTD2",
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 181932
    },
    {
      "department": "DTD2",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 116798
    },
    {
      "department": "DTD3",
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 50041
    },
    {
      "department": "DTD3",
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 82393
    },
    {
      "department": "DTD3",
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 68595
    },
    {
      "department": "DTD3",
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 41910
    },
    {
      "date(Year)": 2014,
      "date(Quarter)": 2,
      "Sum of price": 267598
    },
    {
      "date(Year)": 2014,
      "date(Quarter)": 4,
      "Sum of price": 447141
    },
    {
      "date(Year)": 2014,
      "date(Quarter)": 1,
      "Sum of price": 419727
    },
    {
      "date(Year)": 2014,
      "date(Quarter)": 3,
      "Sum of price": 314958
    },
    {
      "department": "DTDx",
      "employee": "Paul",
      "date(Year)": 2014,
      "Sum of price": 34035
    },
    {
      "department": "DTDx",
      "employee": "Willi",
      "date(Year)": 2014,
      "Sum of price": 27694
    },
    {
      "department": "DTDx",
      "employee": "Winking",
      "date(Year)": 2014,
      "Sum of price": 13758
    },
    {
      "department": "DTDx",
      "employee": "Iver",
      "date(Year)": 2014,
      "Sum of price": 24249
    },
    {
      "department": "DTD1",
      "employee": "Paul",
      "date(Year)": 2014,
      "Sum of price": 185031
    },
    {
      "department": "DTD1",
      "employee": "Willi",
      "date(Year)": 2014,
      "Sum of price": 147403
    },
    {
      "department": "DTD1",
      "employee": "Winking",
      "date(Year)": 2014,
      "Sum of price": 71841
    },
    {
      "department": "DTD1",
      "employee": "Iver",
      "date(Year)": 2014,
      "Sum of price": 102676
    },
    {
      "department": "DTD2",
      "employee": "Paul",
      "date(Year)": 2014,
      "Sum of price": 208934
    },
    {
      "department": "DTD2",
      "employee": "Willi",
      "date(Year)": 2014,
      "Sum of price": 178360
    },
    {
      "department": "DTD2",
      "employee": "Winking",
      "date(Year)": 2014,
      "Sum of price": 96280
    },
    {
      "department": "DTD2",
      "employee": "Iver",
      "date(Year)": 2014,
      "Sum of price": 116224
    },
    {
      "department": "DTD3",
      "employee": "Paul",
      "date(Year)": 2014,
      "Sum of price": 91490
    },
    {
      "department": "DTD3",
      "employee": "Willi",
      "date(Year)": 2014,
      "Sum of price": 62852
    },
    {
      "department": "DTD3",
      "employee": "Winking",
      "date(Year)": 2014,
      "Sum of price": 37268
    },
    {
      "department": "DTD3",
      "employee": "Iver",
      "date(Year)": 2014,
      "Sum of price": 51329
    },
    {
      "department": "DTDx",
      "employee": "Paul",
      "Sum of price": 34035
    },
    {
      "department": "DTDx",
      "employee": "Willi",
      "Sum of price": 27694
    },
    {
      "department": "DTDx",
      "employee": "Winking",
      "Sum of price": 13758
    },
    {
      "department": "DTDx",
      "employee": "Iver",
      "Sum of price": 24249
    },
    {
      "department": "DTD1",
      "employee": "Paul",
      "Sum of price": 185031
    },
    {
      "department": "DTD1",
      "employee": "Willi",
      "Sum of price": 147403
    },
    {
      "department": "DTD1",
      "employee": "Winking",
      "Sum of price": 71841
    },
    {
      "department": "DTD1",
      "employee": "Iver",
      "Sum of price": 102676
    },
    {
      "department": "DTD2",
      "employee": "Paul",
      "Sum of price": 208934
    },
    {
      "department": "DTD2",
      "employee": "Willi",
      "Sum of price": 178360
    },
    {
      "department": "DTD2",
      "employee": "Winking",
      "Sum of price": 96280
    },
    {
      "department": "DTD2",
      "employee": "Iver",
      "Sum of price": 116224
    },
    {
      "department": "DTD3",
      "employee": "Paul",
      "Sum of price": 91490
    },
    {
      "department": "DTD3",
      "employee": "Willi",
      "Sum of price": 62852
    },
    {
      "department": "DTD3",
      "employee": "Winking",
      "Sum of price": 37268
    },
    {
      "department": "DTD3",
      "employee": "Iver",
      "Sum of price": 51329
    },
    {
      "department": "DTDx",
      "date(Year)": 2014,
      "Sum of price": 99736
    },
    {
      "department": "DTD1",
      "date(Year)": 2014,
      "Sum of price": 506951
    },
    {
      "department": "DTD2",
      "date(Year)": 2014,
      "Sum of price": 599798
    },
    {
      "department": "DTD3",
      "date(Year)": 2014,
      "Sum of price": 242939
    },
    {
      "department": "DTDx",
      "Sum of price": 99736
    },
    {
      "department": "DTD1",
      "Sum of price": 506951
    },
    {
      "department": "DTD2",
      "Sum of price": 599798
    },
    {
      "department": "DTD3",
      "Sum of price": 242939
    },
    {
      "date(Year)": 2014,
      "Sum of price": 1449424
    },
    {
      "Sum of price": 1449424
    }
  ]
}
```
