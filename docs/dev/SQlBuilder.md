### CTE Orders For Pivot Model
1. Based on `raw table`, create `extended CTE TMP0` with `dateTimeOffset` field---仅在有calcdatevalue模型内容时需要。
    * DateCalcValue
1. Based on `extended CTE`, create `data_view CTE TMP1` by----仅在有filter时候需要
    * Filter
1. Based on `data_view CTE`, create `context CTE TMP2` includes----仅在value/calcvalue/calcfilter存在时需要， 需要对其运算的原始指标。
    * Colum in Dimension
    * Computational Value
    * Value in PartitionCalcValue
    * Value in PartitionCalcFilter

1. For **Each** Value with `FIRST` method
    1. Based on `data_view CTE`, create `xxx_first CTE  TMP3_n`
1. Append all `xxx_first CTE`s to `context CTE` (INNER JOIN)   TMP4
1. For **Each** Dimension
    1. create `xxx_sort CTE` by orderBy TMP5_n
        * No sort: from `raw table`
        * Sort By value(includes `FIRST`): from `data_view CTE`
        * Otherwise: from `context CTE`
    1. Based on `xxx_sort CTE`, create `xxx_order CTE` (ROW_NUMBER())
1. Append all `xxx_order CTE`s to `context CTE` (INNER JOIN)

1. Apply `context_offset CTE`s (from `dateTimeOffset`) to `context CTE` (LEFT JOIN)

1. For **Each** GroupCalcValue **before** CalcFilter 
    1. Based on `data_view CTE`, create `group_context CTE`
    1. Based on `group_context CTE`, create `group_value CTE`---????? 这个直接把calcvalue给算出来了？？这个和下面的append calculated fields矛盾---算出来
1. Apply `group_value CTE`s to `context CTE` (INNER JOIN)

1. Based on `context CTE`, append calculated fields includes
    * CalcValue that need be done **before** CalcFilter----**Partition的**？？？这个是是不是group和partition的calcvalue都计算？---Partition的
    * CalcValue in PartitionCalcFilter--只是partition情况都做。

1. For **Each** GroupCalcFilter---这个指的是calcfilter中dividedby是group的？--group
    1. Based on `data view CTE`, create `group_context CTE`----这个是相当于生成groupfilter的value，和calcvalue， 需要2次生成table？ 详见0e2cf480-c279-11ea-867f-2de75ef93805-----**这个跟上面的context表是一样的， 只生成到value就行了**
    1. Based on `group_context CTE`, create `group_filter CTE`----**这个实际上是根据上面的表，计算calcfilter用到的clacvalue而已，没有where条件。**
1. Apply `group_filter CTE`s to `context CTE` (INNER JOIN + WHERE)------group_filter CTE 这个已经加了group filter的过滤， 为啥这里还得加where？**---这里添加groupcalcFilter的where条件**
1. Based on `context CTE`, reduce rows by  ---- to filter new context cte
    * PartitionRankFilter  ---这个的calcvalue在上面before的append calc fields的步骤已经存在了。 只要对calcvalue做filter就好。**---注意rank的percentage情况**
    * PartitionValueFilter--- 已经没有这个叫法的了。 现在是哪个？identityvalue的这个。

1. Based on `context CTE`, append calculated fields includes
    * PartitionCalcValue that need be done **after** CalcFilter

1. Apply `context CTE` to `data_view CTE`, get `data_view_with_filter CTE` (INNER JOIN)---context cte is used to filter data_view CTE ,inner join dimension=dimension.
1. For **Each** GroupCalcValue **after** CalcFilter
    1. Based on `data_view_with_filter CTE`, create `group_context CTE` ---add dimensions, and value of calcvalue 
    1. Based on `group_context CTE`, create `group_value CTE`
1. Append `group_value CTE`s to `context CTE` (INNER JOIN)

1. Based on `context CTE`, handle some special cases includes:
    * Change rank value to `Null` when the original value is `Null`---是说rankvalue的value是null的情况
1. Based on `context CTE`, get results includes
select dimension1, dimension1_order ,dimension2，dimension2_order, value,calcvalue
from table
    * Column in Dimension (*xxx*)
    * orderBy in Dimension (*xxx_order*)
    * Value (*sum_of_xxx*)
    * CalcValue (*running_sum_on_xxx*)

    针对测试的case：select dimension1,, value,calcvalue
from table order by  dimension1_order asc, dimension2_order  asc  . 但仅是针对测试， 上线的上面的那个sql。


### Attention
1. For average accumulation on average aggregation, use `CASE(SUM(SUM(...) AS DOUBLE))/SUM(COUNT(*))` instead
1. For `DateCalcValue`, build two individual contexts, one for current subdate, the other for shifted subdate. combine two contexts by `LEFT JOIN`
1. Able to get `calculate CTE` before/after applying `calcFilter CTE`
1. For `Distinct Count`, create `xxx_sort CTE` on `data_view CTE` not `context CTE`
1. For `Distinct Count`, create `group_filter CTE` on `data_view CTE` not `context CTE`
1. For value with `FIRST` method, create an individual CTE.
1. For all `INNER JOIN`, MUST specify null value equals for all nullable field in join conditions
1. No sort: distinct from `raw table` (before filters)
1. Sort by value: from `data_view CTE` (after filters)
1. For Rank value, null value should produce null rank

### Optimization
1. For model with only one dimension, extend `xxx_order` field on `context CTE` directly. (remove JOIN)
1. Maybe we can use `temp table` to speed up multiple queries which based on similar context. (PivotTable)
1. For group filter divide by single column, use `WHERE IN (subQuery)` rather than `INNER JOIN`
1. For group calc value after calc filter, roll-up value on `context CTE` rather than `INNER JOIN` with `data view CTE` if possible

### Limitation
1. The `divideBy columns` is a subset of `dimensions`
1. In the window function of `AccumulationValue` (running/moving), order by all `Column`s within the set difference of `dimensions` and `divideBy columns`.

### Raw Model
1. Single query is enough for a raw model.
1. Don't add order field for each dimension.
1. Not support sort dimension by value but `FIRST` type
1. Sort by `FIRST` equals to sort by column