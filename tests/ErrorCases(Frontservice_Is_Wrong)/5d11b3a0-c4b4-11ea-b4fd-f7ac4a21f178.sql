 WITH T_Extened_10a4fd AS (
SELECT  *
FROM voyages ) 

, T_Filter_28c4c1 AS (
SELECT  *
FROM T_Extened_10a4fd
WHERE ((EXTRACT(Year 
FROM "departure_date") = 1595 OR EXTRACT(Year
FROM "departure_date") = 1598 OR EXTRACT(Year
FROM "departure_date") = 1599 OR EXTRACT(Year
FROM "departure_date") = 1600 OR EXTRACT(Year
FROM "departure_date") = 1601 OR EXTRACT(Year
FROM "departure_date") = 1602 OR EXTRACT(Year
FROM "departure_date") = 1611)) ) 

, T_Context_9d5421 AS (
SELECT  EXTRACT(Year
FROM "departure_date") AS "departure_date_Year" ,
 "arrival_harbour" AS "arrival_harbour" , 
 SUM("trip") AS "trip_SUM1088272477"
FROM T_Filter_28c4c1
GROUP BY  "departure_date_Year" 
         ,"arrival_harbour" ) 


,T_Sort_e3cc22 AS (
SELECT  "departure_date_Year" AS "departure_date_Year"
FROM T_Context_9d5421
GROUP BY  "departure_date_Year"
ORDER BY 1 Asc ) 


,T_Sort_1ddd00 AS (
SELECT  DISTINCT "arrival_harbour" AS "arrival_harbour"
FROM T_Extened_10a4fd ) 

, T_Order_89f265 AS (
SELECT  "departure_date_Year" AS "departure_date_Year" 
       ,ROW_NUMBER() OVER()   AS "departure_date_Year_ORDER"
FROM T_Sort_e3cc22 ) 

, T_Order_155974 AS (
SELECT  "arrival_harbour"   AS "arrival_harbour" 
       ,ROW_NUMBER() OVER() AS "arrival_harbour_ORDER"
FROM T_Sort_1ddd00 ) 

, T_OrderJoin_0157d0 AS (
SELECT  T_Context_9d5421."departure_date_Year" AS "departure_date_Year" 
       ,T_Context_9d5421."arrival_harbour"     AS "arrival_harbour" 
       ,T_Context_9d5421."trip_SUM1088272477"  AS "trip_SUM1088272477" 
       ,"departure_date_Year_ORDER"            AS "departure_date_Year_ORDER" 
       ,"arrival_harbour_ORDER"                AS "arrival_harbour_ORDER"
FROM T_Context_9d5421
INNER JOIN T_Order_89f265
ON (T_Context_9d5421."departure_date_Year" = T_Order_89f265."departure_date_Year" OR (T_Context_9d5421."departure_date_Year" IS NULL AND T_Order_89f265."departure_date_Year" IS NULL ))
INNER JOIN T_Order_155974
ON (T_Context_9d5421."arrival_harbour" = T_Order_155974."arrival_harbour" OR (T_Context_9d5421."arrival_harbour" IS NULL AND T_Order_155974."arrival_harbour" IS NULL )) ) 


, T_AppendCalcValue_Before_Original_5d8a42 AS (
SELECT  T_OrderJoin_0157d0."departure_date_Year"       AS "departure_date_Year" 
       ,T_OrderJoin_0157d0."arrival_harbour"           AS "arrival_harbour" 
       ,T_OrderJoin_0157d0."trip_SUM1088272477"        AS "trip_SUM1088272477" 
       ,T_OrderJoin_0157d0."departure_date_Year_ORDER" AS "departure_date_Year_ORDER" 
       ,T_OrderJoin_0157d0."arrival_harbour_ORDER"     AS "arrival_harbour_ORDER"
FROM T_OrderJoin_0157d0 ) 

, T_ContextReduce_f8b3b1 AS (
SELECT  T_AppendCalcValue_Before_Original_5d8a42."departure_date_Year"       AS "departure_date_Year" 
       ,T_AppendCalcValue_Before_Original_5d8a42."arrival_harbour"           AS "arrival_harbour" 
       ,T_AppendCalcValue_Before_Original_5d8a42."trip_SUM1088272477"        AS "trip_SUM1088272477" 
       ,T_AppendCalcValue_Before_Original_5d8a42."departure_date_Year_ORDER" AS "departure_date_Year_ORDER" 
       ,T_AppendCalcValue_Before_Original_5d8a42."arrival_harbour_ORDER"     AS "arrival_harbour_ORDER"
FROM T_AppendCalcValue_Before_Original_5d8a42 ) 

, T_AppendCalcValue_After_Original_11b895 AS (
SELECT  T_ContextReduce_f8b3b1."departure_date_Year"           AS "departure_date_Year" 
       ,T_ContextReduce_f8b3b1."arrival_harbour"               AS "arrival_harbour" 
       ,T_ContextReduce_f8b3b1."trip_SUM1088272477"            AS "trip_SUM1088272477" 
       ,T_ContextReduce_f8b3b1."departure_date_Year_ORDER"     AS "departure_date_Year_ORDER" 
       ,T_ContextReduce_f8b3b1."arrival_harbour_ORDER"         AS "arrival_harbour_ORDER" 
       ,ROW_NUMBER() OVER( ORDER BY "trip_SUM1088272477" Desc) AS "trip_SUM126114282_Rank764788668"
FROM T_ContextReduce_f8b3b1 ) 

, T_ContextHandle_c0df2f AS (
SELECT  T_AppendCalcValue_After_Original_11b895."departure_date_Year"                               AS "departure_date_Year" 
       ,T_AppendCalcValue_After_Original_11b895."arrival_harbour"                                   AS "arrival_harbour" 
       ,T_AppendCalcValue_After_Original_11b895."trip_SUM1088272477"                                AS "trip_SUM1088272477" 
       ,T_AppendCalcValue_After_Original_11b895."departure_date_Year_ORDER"                         AS "departure_date_Year_ORDER" 
       ,T_AppendCalcValue_After_Original_11b895."arrival_harbour_ORDER"                             AS "arrival_harbour_ORDER" 
       ,T_AppendCalcValue_After_Original_11b895."trip_SUM126114282_Rank764788668"                   AS "trip_SUM126114282_Rank764788668" 
       ,CASE WHEN "trip_SUM1088272477" IS NULL THEN NULL ELSE "trip_SUM126114282_Rank764788668" END AS "trip_Sum_Rank91972817"
FROM T_AppendCalcValue_After_Original_11b895 )


SELECT  "departure_date_Year" 
       ,"arrival_harbour" 
       ,"trip_Sum_Rank91972817"
FROM T_ContextHandle_c0df2f
ORDER BY "departure_date_Year_ORDER" ,"arrival_harbour_ORDER" ;