SELECT "cape_arrival" AS "cape_arrival" , "number" AS "number" , "trip" AS "trip" , "arrival_harbour" AS "arrival_harbour"
 FROM voyages
 WHERE ( ( EXTRACT(Year FROM "cape_arrival")  >= 1192 AND EXTRACT(Year FROM "cape_arrival")  <= 1703 )  And  not ( EXTRACT(Year FROM "cape_arrival")  >= 1665 AND EXTRACT(Year FROM "cape_arrival")  <= 1667 ) And  (EXTRACT(Year FROM "cape_arrival")  <= 1670 )  And  (EXTRACT(Year FROM "cape_arrival")  < 1670 )  And  ( EXTRACT(Year FROM "cape_arrival")  >= 1652)  And  ( EXTRACT(Year FROM "cape_arrival")  > 1652)  And (EXTRACT(Year FROM "cape_arrival")  <> 1654) And (EXTRACT(Year FROM "cape_arrival")  = 1655)) 
 ORDER BY "cape_arrival" Asc , "arrival_harbour" Desc
 LIMIT 200 
 OFFSET 0 ;