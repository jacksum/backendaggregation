 WITH T_Extened_3ffd63 AS (
SELECT  *  ,"departure_date" - INTERVAL '1' Year AS "departure_date_OFFSET1833860460"
FROM voyages ) 
, T_Filter_bfe59e AS (
SELECT  *
FROM T_Extened_3ffd63
WHERE ((EXTRACT(Year 
FROM "departure_date") = 1598 OR EXTRACT(Year
FROM "departure_date") = 1599 OR EXTRACT(Year
FROM "departure_date") = 1600 OR EXTRACT(Year
FROM "departure_date") = 1601 OR EXTRACT(Year
FROM "departure_date") = 1602 OR EXTRACT(Year
FROM "departure_date") = 1603
 OR EXTRACT(Year
FROM "departure_date") = 1604)
 AND ( "departure_date" >= sys.str_to_date('1599-01-01T00:00:00', '%Y-%m-%dT%H:%M:%S') AND "departure_date" <= sys.str_to_date('1603-12-31T23:59:59', '%Y-%m-%dT%H:%M:%S') ) ) ) 
 
 , T_FilterOffset_81701b_departure_date_OFFSET1833860460 AS (
SELECT  *
FROM T_Extened_3ffd63
WHERE ((EXTRACT(Year 
FROM "departure_date_OFFSET1833860460") = 1598 OR EXTRACT(Year
FROM "departure_date_OFFSET1833860460") = 1599 OR EXTRACT(Year
FROM "departure_date_OFFSET1833860460") = 1600 OR EXTRACT(Year
FROM "departure_date_OFFSET1833860460") = 1601 OR EXTRACT(Year
FROM "departure_date_OFFSET1833860460") = 1602 OR EXTRACT(Year
FROM "departure_date_OFFSET1833860460") = 1603 OR EXTRACT(Year
FROM "departure_date_OFFSET1833860460") = 1604
) AND ( "departure_date_OFFSET1833860460" >= sys.str_to_date('1599-01-01T00:00:00', '%Y-%m-%dT%H:%M:%S') AND "departure_date_OFFSET1833860460" <= sys.str_to_date('1603-12-31T23:59:59', '%Y-%m-%dT%H:%M:%S') ) ) ) 

, T_Context_03472f AS (
SELECT  EXTRACT(Year
FROM "departure_date") AS "departure_date_Year" , SUM("trip") AS "trip_SUM1673605968"
FROM T_Filter_bfe59e
GROUP BY  "departure_date_Year" ) 

,T_ContextOffset_d118af AS (
SELECT  EXTRACT(Year
FROM "departure_date_OFFSET1833860460") AS "departure_date_OFFSET1833860460_Year" ,
 SUM("trip") AS "trip_SUM1664471691"
FROM T_FilterOffset_81701b_departure_date_OFFSET1833860460
GROUP BY  "departure_date_OFFSET1833860460_Year" ) 

,T_Sort_177aae AS (
SELECT  "departure_date_Year" AS "departure_date_Year"
FROM T_Context_03472f
GROUP BY  "departure_date_Year"
ORDER BY 1 Asc ) 

,T_Order_62038d AS (
SELECT  "departure_date_Year" AS "departure_date_Year" 
       ,ROW_NUMBER() OVER()   AS "departure_date_Year_ORDER"
FROM T_Sort_177aae ) 

, T_OrderJoin_9d62b7 AS (
SELECT  T_Context_03472f."departure_date_Year" AS "departure_date_Year" 
       ,T_Context_03472f."trip_SUM1673605968"  AS "trip_SUM1673605968" 
       ,"departure_date_Year_ORDER"            AS "departure_date_Year_ORDER"
FROM T_Context_03472f
INNER JOIN T_Order_62038d
ON (T_Context_03472f."departure_date_Year" = T_Order_62038d."departure_date_Year" OR (T_Context_03472f."departure_date_Year" IS NULL AND T_Order_62038d."departure_date_Year" IS NULL )) ) 

, T_OffsetLeftJoin_468fd6 AS (
SELECT  T_OrderJoin_9d62b7."departure_date_Year"                                                              AS "departure_date_Year" 
       ,T_OrderJoin_9d62b7."trip_SUM1673605968"                                                               AS "trip_SUM1673605968" 
       ,T_ContextOffset_d118af."trip_SUM1664471691" as "trip_SUM1664471691"
       ,T_OrderJoin_9d62b7."departure_date_Year_ORDER"                                                        AS "departure_date_Year_ORDER" 
       ,CAST((T_OrderJoin_9d62b7."trip_SUM1673605968"-T_ContextOffset_d118af."trip_SUM1664471691") 
       AS DOUBLE) AS "trip_SUM1664471691_DateTimeOffset2131162891"
FROM T_OrderJoin_9d62b7
LEFT JOIN T_ContextOffset_d118af
ON ((T_OrderJoin_9d62b7."departure_date_Year" = T_ContextOffset_d118af."departure_date_OFFSET1833860460_Year" 
OR (T_OrderJoin_9d62b7."departure_date_Year" IS NULL AND T_ContextOffset_d118af."departure_date_OFFSET1833860460_Year" IS NULL)
)) ) 

, T_AppendCalcValue_Before_Original_fcc459 AS (
SELECT  T_OffsetLeftJoin_468fd6."departure_date_Year"                         AS "departure_date_Year" 
       ,T_OffsetLeftJoin_468fd6."trip_SUM1673605968"                          AS "trip_SUM1673605968" 
       ,"trip_SUM1664471691"
       ,T_OffsetLeftJoin_468fd6."departure_date_Year_ORDER"                   AS "departure_date_Year_ORDER" 
       ,T_OffsetLeftJoin_468fd6."trip_SUM1664471691_DateTimeOffset2131162891" AS "trip_SUM1664471691_DateTimeOffset2131162891"
FROM T_OffsetLeftJoin_468fd6 ) 

, T_ContextReduce_744561 AS (
SELECT  T_AppendCalcValue_Before_Original_fcc459."departure_date_Year"                         AS "departure_date_Year" 
       ,T_AppendCalcValue_Before_Original_fcc459."trip_SUM1673605968"                          AS "trip_SUM1673605968" 
       ,"trip_SUM1664471691"
       ,T_AppendCalcValue_Before_Original_fcc459."departure_date_Year_ORDER"                   AS "departure_date_Year_ORDER" 
       ,T_AppendCalcValue_Before_Original_fcc459."trip_SUM1664471691_DateTimeOffset2131162891" AS "trip_SUM1664471691_DateTimeOffset2131162891"
FROM T_AppendCalcValue_Before_Original_fcc459 ) 

, T_AppendCalcValue_After_Original_c8c24c AS (
SELECT  T_ContextReduce_744561."departure_date_Year"                         AS "departure_date_Year" 
       ,T_ContextReduce_744561."trip_SUM1673605968"                          AS "trip_SUM1673605968" 
       ,"trip_SUM1664471691"
       ,T_ContextReduce_744561."departure_date_Year_ORDER"                   AS "departure_date_Year_ORDER" 
       ,T_ContextReduce_744561."trip_SUM1664471691_DateTimeOffset2131162891" AS "trip_SUM1664471691_DateTimeOffset2131162891"
FROM T_ContextReduce_744561 )
SELECT  "departure_date_Year" 
       ,"trip_SUM1664471691_DateTimeOffset2131162891"
FROM T_AppendCalcValue_After_Original_c8c24c
order by "departure_date_Year_ORDER"  ;