 WITH T_Extened_a22110 AS (
SELECT  *
FROM voyages ) , T_Filter_4b6960 AS (
SELECT  *
FROM T_Extened_a22110
WHERE ((EXTRACT(Year 
FROM "departure_date") = 1595 OR EXTRACT(Year
FROM "departure_date") = 1598 OR EXTRACT(Year
FROM "departure_date") = 1599 OR EXTRACT(Year
FROM "departure_date") = 1600 OR EXTRACT(Year
FROM "departure_date") = 1601 OR EXTRACT(Year
FROM "departure_date") = 1611) AND ("arrival_harbour" = 'Engano' OR "arrival_harbour" = 'Bantam' OR "arrival_harbour" = 'Atjeh' OR "arrival_harbour" = 'Tidore' OR "arrival_harbour" = 'Ternate' OR "arrival_harbour" = 'Djaratan' OR "arrival_harbour" = 'Ceylon' OR "arrival_harbour" = 'Madagascar' OR "arrival_harbour" = 'Cambodia' OR "arrival_harbour" = 'Johore')) ) , T_Context_05a9ae AS (
SELECT  EXTRACT(Year
FROM "departure_date") AS "departure_date_Year" , EXTRACT(Month
FROM "departure_date") AS "departure_date_Month" , EXTRACT(Quarter
FROM "departure_date") AS "departure_date_Quarter" , MAX("number") AS "number_MAX785653213" , AVG("trip") AS "trip_AVG385999778" , SUM("trip") AS "trip_SUM1841172284"
FROM T_Filter_4b6960
GROUP BY  "departure_date_Year" 
         ,"departure_date_Month" 
         ,"departure_date_Quarter" ) 
         ,T_Sort_b7ff19 AS (
SELECT  "departure_date_Year" AS "departure_date_Year"
FROM T_Context_05a9ae
GROUP BY  "departure_date_Year"
ORDER BY 1 Asc ) 
         ,T_Sort_29591e AS (
SELECT  "departure_date_Month" AS "departure_date_Month"
FROM T_Context_05a9ae
GROUP BY  "departure_date_Month"
ORDER BY (CASE WHEN "departure_date_Month" = '3' THEN 0 WHEN "departure_date_Month" = '5' THEN 1 WHEN "departure_date_Month" = '6' THEN 2 WHEN "departure_date_Month" IS NULL THEN 3 WHEN "departure_date_Month" = '4' THEN 4 WHEN "departure_date_Month" = '12' THEN 5 WHEN "departure_date_Month" = '1' THEN 6 WHEN "departure_date_Month" = '7' THEN 7 WHEN "departure_date_Month" = '8' THEN 8 WHEN "departure_date_Month" = '9' THEN 9 WHEN "departure_date_Month" = '2' THEN 10 WHEN "departure_date_Month" = '10' THEN 11 WHEN "departure_date_Month" = '11' THEN 12 ELSE 13 END) ASC ) 
         ,T_Sort_be587e AS (
SELECT  EXTRACT(Quarter
FROM "departure_date") AS "departure_date_Quarter" , MAX("number") AS "number_MAX1810210569"
FROM T_Filter_4b6960
GROUP BY  "departure_date_Quarter"
ORDER BY 2 Desc ) 
         ,T_Order_c42881 AS (
SELECT  "departure_date_Year" AS "departure_date_Year" 
       ,ROW_NUMBER() OVER()   AS "departure_date_Year_ORDER"
FROM T_Sort_b7ff19 ) , T_Order_afaf31 AS (
SELECT  "departure_date_Month" AS "departure_date_Month" 
       ,ROW_NUMBER() OVER()    AS "departure_date_Month_ORDER"
FROM T_Sort_29591e ) , T_Order_9ce539 AS (
SELECT  "departure_date_Quarter" AS "departure_date_Quarter" 
       ,ROW_NUMBER() OVER()      AS "departure_date_Quarter_ORDER"
FROM T_Sort_be587e ) , T_OrderJoin_94a3de AS (
SELECT  T_Context_05a9ae."departure_date_Year"    AS "departure_date_Year" 
       ,T_Context_05a9ae."departure_date_Month"   AS "departure_date_Month" 
       ,T_Context_05a9ae."departure_date_Quarter" AS "departure_date_Quarter" 
       ,T_Context_05a9ae."number_MAX785653213"    AS "number_MAX785653213" 
       ,T_Context_05a9ae."trip_AVG385999778"      AS "trip_AVG385999778" 
       ,T_Context_05a9ae."trip_SUM1841172284"     AS "trip_SUM1841172284" 
       ,"departure_date_Year_ORDER"               AS "departure_date_Year_ORDER" 
       ,"departure_date_Month_ORDER"              AS "departure_date_Month_ORDER" 
       ,"departure_date_Quarter_ORDER"            AS "departure_date_Quarter_ORDER"
FROM T_Context_05a9ae
INNER JOIN T_Order_c42881
ON (T_Context_05a9ae."departure_date_Year" = T_Order_c42881."departure_date_Year" OR (T_Context_05a9ae."departure_date_Year" IS NULL AND T_Order_c42881."departure_date_Year" IS NULL ))
INNER JOIN T_Order_afaf31
ON (T_Context_05a9ae."departure_date_Month" = T_Order_afaf31."departure_date_Month" OR (T_Context_05a9ae."departure_date_Month" IS NULL AND T_Order_afaf31."departure_date_Month" IS NULL ))
INNER JOIN T_Order_9ce539
ON (T_Context_05a9ae."departure_date_Quarter" = T_Order_9ce539."departure_date_Quarter" OR (T_Context_05a9ae."departure_date_Quarter" IS NULL AND T_Order_9ce539."departure_date_Quarter" IS NULL )) ) , T_AppendCalcValue_Before_Original_9bc1fa AS (
SELECT  T_OrderJoin_94a3de."departure_date_Year"                                                                            AS "departure_date_Year" 
       ,T_OrderJoin_94a3de."departure_date_Month"                                                                           AS "departure_date_Month" 
       ,T_OrderJoin_94a3de."departure_date_Quarter"                                                                         AS "departure_date_Quarter" 
       ,T_OrderJoin_94a3de."number_MAX785653213"                                                                            AS "number_MAX785653213" 
       ,T_OrderJoin_94a3de."trip_AVG385999778"                                                                              AS "trip_AVG385999778" 
       ,T_OrderJoin_94a3de."trip_SUM1841172284"                                                                             AS "trip_SUM1841172284" 
       ,T_OrderJoin_94a3de."departure_date_Year_ORDER"                                                                      AS "departure_date_Year_ORDER" 
       ,T_OrderJoin_94a3de."departure_date_Month_ORDER"                                                                     AS "departure_date_Month_ORDER" 
       ,T_OrderJoin_94a3de."departure_date_Quarter_ORDER"                                                                   AS "departure_date_Quarter_ORDER" 
       ,ROW_NUMBER() OVER(PARTITION BY "departure_date_Month" ,"departure_date_Quarter" ORDER BY "trip_SUM1841172284" Desc) AS "trip_SUM2033290443_Rank_FILTER1761323626"
FROM T_OrderJoin_94a3de ) , T_ContextReduce_798e55 AS (
SELECT  T_AppendCalcValue_Before_Original_9bc1fa."departure_date_Year"                      AS "departure_date_Year" 
       ,T_AppendCalcValue_Before_Original_9bc1fa."departure_date_Month"                     AS "departure_date_Month" 
       ,T_AppendCalcValue_Before_Original_9bc1fa."departure_date_Quarter"                   AS "departure_date_Quarter" 
       ,T_AppendCalcValue_Before_Original_9bc1fa."number_MAX785653213"                      AS "number_MAX785653213" 
       ,T_AppendCalcValue_Before_Original_9bc1fa."trip_AVG385999778"                        AS "trip_AVG385999778" 
       ,T_AppendCalcValue_Before_Original_9bc1fa."trip_SUM1841172284"                       AS "trip_SUM1841172284" 
       ,T_AppendCalcValue_Before_Original_9bc1fa."departure_date_Year_ORDER"                AS "departure_date_Year_ORDER" 
       ,T_AppendCalcValue_Before_Original_9bc1fa."departure_date_Month_ORDER"               AS "departure_date_Month_ORDER" 
       ,T_AppendCalcValue_Before_Original_9bc1fa."departure_date_Quarter_ORDER"             AS "departure_date_Quarter_ORDER" 
       ,T_AppendCalcValue_Before_Original_9bc1fa."trip_SUM2033290443_Rank_FILTER1761323626" AS "trip_SUM2033290443_Rank_FILTER1761323626"
FROM T_AppendCalcValue_Before_Original_9bc1fa
WHERE ( "trip_SUM2033290443_Rank_FILTER1761323626" >= 0 AND "trip_SUM2033290443_Rank_FILTER1761323626" <= 2 ) ) , T_AppendCalcValue_After_Original_c6cfbb AS ( 
SELECT  T_ContextReduce_798e55."departure_date_Year"                      AS "departure_date_Year" 
       ,T_ContextReduce_798e55."departure_date_Month"                     AS "departure_date_Month" 
       ,T_ContextReduce_798e55."departure_date_Quarter"                   AS "departure_date_Quarter" 
       ,T_ContextReduce_798e55."number_MAX785653213"                      AS "number_MAX785653213" 
       ,T_ContextReduce_798e55."trip_AVG385999778"                        AS "trip_AVG385999778" 
       ,T_ContextReduce_798e55."trip_SUM1841172284"                       AS "trip_SUM1841172284" 
       ,T_ContextReduce_798e55."departure_date_Year_ORDER"                AS "departure_date_Year_ORDER" 
       ,T_ContextReduce_798e55."departure_date_Month_ORDER"               AS "departure_date_Month_ORDER" 
       ,T_ContextReduce_798e55."departure_date_Quarter_ORDER"             AS "departure_date_Quarter_ORDER" 
       ,T_ContextReduce_798e55."trip_SUM2033290443_Rank_FILTER1761323626" AS "trip_SUM2033290443_Rank_FILTER1761323626" 
       ,SUM("trip_AVG385999778") OVER( ORDER BY "departure_date_Year_ORDER" ASC ,"departure_date_Month_ORDER" ASC ,"departure_date_Quarter_ORDER" ASC ROWS BETWEEN 2 PRECEDING AND 0 FOLLOWING) AS "trip_AVG385999778_Accumulation_Moving160211855"
FROM T_ContextReduce_798e55 )
SELECT  "departure_date_Year"       
       ,"departure_date_Month" 
       ,"departure_date_Quarter" 
       ,"number_MAX785653213" 
       ,"trip_AVG385999778_Accumulation_Moving160211855"
FROM T_AppendCalcValue_After_Original_c6cfbb
order by  "departure_date_Year_ORDER" 
       ,"departure_date_Month_ORDER" 
       ,"departure_date_Quarter_ORDER" 
 ;