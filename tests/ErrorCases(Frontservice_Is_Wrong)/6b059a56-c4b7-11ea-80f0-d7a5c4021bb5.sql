 WITH T_Extened_c5fc6e AS (
SELECT  * 
       ,"departure_date" + INTERVAL '6' Month AS "departure_date_OFFSET1298081719"
FROM voyages ) , T_Filter_db99b0 AS (
SELECT  *
FROM T_Extened_c5fc6e
WHERE ((EXTRACT(Year 
FROM "departure_date") = 1598 OR EXTRACT(Year
FROM "departure_date") = 1599 OR EXTRACT(Year
FROM "departure_date") = 1600 OR EXTRACT(Year
FROM "departure_date") = 1601 OR EXTRACT(Year
FROM "departure_date") = 1602 OR EXTRACT(Year
FROM "departure_date") = 1603) AND ( "departure_date" >= sys.str_to_date('1602-10-01T00:00:00', '%Y-%m-%dT%H:%M:%S') AND "departure_date" <= sys.str_to_date('1602-12-13T23:59:59', '%Y-%m-%dT%H:%M:%S') ) ) ) , T_FilterOffset_beefa9_departure_date_OFFSET1298081719 AS (
SELECT  *
FROM T_Extened_c5fc6e
WHERE ((EXTRACT(Year 
FROM "departure_date_OFFSET1298081719") = 1598 OR EXTRACT(Year
FROM "departure_date_OFFSET1298081719") = 1599 OR EXTRACT(Year
FROM "departure_date_OFFSET1298081719") = 1600 OR EXTRACT(Year
FROM "departure_date_OFFSET1298081719") = 1601 OR EXTRACT(Year
FROM "departure_date_OFFSET1298081719") = 1602 OR EXTRACT(Year
FROM "departure_date_OFFSET1298081719") = 1603) AND ( "departure_date_OFFSET1298081719" >= sys.str_to_date('1602-10-01T00:00:00', '%Y-%m-%dT%H:%M:%S') AND "departure_date_OFFSET1298081719" <= sys.str_to_date('1602-12-13T23:59:59', '%Y-%m-%dT%H:%M:%S') ) ) ) , T_Context_4b9941 AS (
SELECT  EXTRACT(Year
FROM "departure_date") AS "departure_date_Year" , EXTRACT(Quarter
FROM "departure_date") AS "departure_date_Quarter" , SUM("trip") AS "trip_SUM588040374"
FROM T_Filter_db99b0
GROUP BY  "departure_date_Year" 
         ,"departure_date_Quarter" ) 
         ,T_ContextOffset_ec899d AS (
SELECT  EXTRACT(Year
FROM "departure_date_OFFSET1298081719") AS "departure_date_OFFSET1298081719_Year" , EXTRACT(Quarter
FROM "departure_date_OFFSET1298081719") AS "departure_date_OFFSET1298081719_Quarter" , SUM("trip") AS "trip_SUM191793929"
FROM T_FilterOffset_beefa9_departure_date_OFFSET1298081719
GROUP BY  "departure_date_OFFSET1298081719_Year" 
         ,"departure_date_OFFSET1298081719_Quarter" ) 
         ,T_Sort_9b3688 AS (
SELECT  "departure_date_Year" AS "departure_date_Year"
FROM T_Context_4b9941
GROUP BY  "departure_date_Year"
ORDER BY 1 Asc ) 
         ,T_Sort_6e02f3 AS (
SELECT  "departure_date_Quarter" AS "departure_date_Quarter"
FROM T_Context_4b9941
GROUP BY  "departure_date_Quarter"
ORDER BY 1 Asc ) 
         ,T_Order_aabd1d AS (
SELECT  "departure_date_Year" AS "departure_date_Year" 
       ,ROW_NUMBER() OVER()   AS "departure_date_Year_ORDER"
FROM T_Sort_9b3688 ) , T_Order_2a061c AS (
SELECT  "departure_date_Quarter" AS "departure_date_Quarter" 
       ,ROW_NUMBER() OVER()      AS "departure_date_Quarter_ORDER"
FROM T_Sort_6e02f3 ) , T_OrderJoin_7a7da1 AS (
SELECT  T_Context_4b9941."departure_date_Year"    AS "departure_date_Year" 
       ,T_Context_4b9941."departure_date_Quarter" AS "departure_date_Quarter" 
       ,T_Context_4b9941."trip_SUM588040374"      AS "trip_SUM588040374" 
       ,"departure_date_Year_ORDER"               AS "departure_date_Year_ORDER" 
       ,"departure_date_Quarter_ORDER"            AS "departure_date_Quarter_ORDER"
FROM T_Context_4b9941
INNER JOIN T_Order_aabd1d
ON (T_Context_4b9941."departure_date_Year" = T_Order_aabd1d."departure_date_Year" OR (T_Context_4b9941."departure_date_Year" IS NULL AND T_Order_aabd1d."departure_date_Year" IS NULL ))
INNER JOIN T_Order_2a061c
ON (T_Context_4b9941."departure_date_Quarter" = T_Order_2a061c."departure_date_Quarter" OR (T_Context_4b9941."departure_date_Quarter" IS NULL AND T_Order_2a061c."departure_date_Quarter" IS NULL )) ) , T_OffsetLeftJoin_a7dfe0 AS (
SELECT  T_OrderJoin_7a7da1."departure_date_Year"          AS "departure_date_Year" 
       ,T_OrderJoin_7a7da1."departure_date_Quarter"       AS "departure_date_Quarter" 
       ,T_OrderJoin_7a7da1."trip_SUM588040374"            AS "trip_SUM588040374" 
       ,T_OrderJoin_7a7da1."departure_date_Year_ORDER"    AS "departure_date_Year_ORDER" 
       ,T_OrderJoin_7a7da1."departure_date_Quarter_ORDER" AS "departure_date_Quarter_ORDER" 
       ,T_ContextOffset_ec899d."trip_SUM191793929"        AS "trip_SUM191793929_DateTimeOffset327175942"
FROM T_OrderJoin_7a7da1
LEFT JOIN T_ContextOffset_ec899d
ON ((T_OrderJoin_7a7da1."departure_date_Year" = T_ContextOffset_ec899d."departure_date_OFFSET1298081719_Year" OR (T_OrderJoin_7a7da1."departure_date_Year" IS NULL AND T_ContextOffset_ec899d."departure_date_OFFSET1298081719_Year" IS NULL)) AND (T_OrderJoin_7a7da1."departure_date_Quarter" = T_ContextOffset_ec899d."departure_date_OFFSET1298081719_Quarter" OR (T_OrderJoin_7a7da1."departure_date_Quarter" IS NULL AND T_ContextOffset_ec899d."departure_date_OFFSET1298081719_Quarter" IS NULL))) ) , T_AppendCalcValue_Before_Original_7dc1da AS (
SELECT  T_OffsetLeftJoin_a7dfe0."departure_date_Year"                       AS "departure_date_Year" 
       ,T_OffsetLeftJoin_a7dfe0."departure_date_Quarter"                    AS "departure_date_Quarter" 
       ,T_OffsetLeftJoin_a7dfe0."trip_SUM588040374"                         AS "trip_SUM588040374" 
       ,T_OffsetLeftJoin_a7dfe0."departure_date_Year_ORDER"                 AS "departure_date_Year_ORDER" 
       ,T_OffsetLeftJoin_a7dfe0."departure_date_Quarter_ORDER"              AS "departure_date_Quarter_ORDER" 
       ,T_OffsetLeftJoin_a7dfe0."trip_SUM191793929_DateTimeOffset327175942" AS "trip_SUM191793929_DateTimeOffset327175942"
FROM T_OffsetLeftJoin_a7dfe0 ) , T_ContextReduce_67999d AS (
SELECT  T_AppendCalcValue_Before_Original_7dc1da."departure_date_Year"                       AS "departure_date_Year" 
       ,T_AppendCalcValue_Before_Original_7dc1da."departure_date_Quarter"                    AS "departure_date_Quarter" 
       ,T_AppendCalcValue_Before_Original_7dc1da."trip_SUM588040374"                         AS "trip_SUM588040374" 
       ,T_AppendCalcValue_Before_Original_7dc1da."departure_date_Year_ORDER"                 AS "departure_date_Year_ORDER" 
       ,T_AppendCalcValue_Before_Original_7dc1da."departure_date_Quarter_ORDER"              AS "departure_date_Quarter_ORDER" 
       ,T_AppendCalcValue_Before_Original_7dc1da."trip_SUM191793929_DateTimeOffset327175942" AS "trip_SUM191793929_DateTimeOffset327175942"
FROM T_AppendCalcValue_Before_Original_7dc1da ) , T_AppendCalcValue_After_Original_5f5ba4 AS (
SELECT  T_ContextReduce_67999d."departure_date_Year"                       AS "departure_date_Year" 
       ,T_ContextReduce_67999d."departure_date_Quarter"                    AS "departure_date_Quarter" 
       ,T_ContextReduce_67999d."trip_SUM588040374"                         AS "trip_SUM588040374" 
       ,T_ContextReduce_67999d."departure_date_Year_ORDER"                 AS "departure_date_Year_ORDER" 
       ,T_ContextReduce_67999d."departure_date_Quarter_ORDER"              AS "departure_date_Quarter_ORDER" 
       ,T_ContextReduce_67999d."trip_SUM191793929_DateTimeOffset327175942" AS "trip_SUM191793929_DateTimeOffset327175942"
FROM T_ContextReduce_67999d )
SELECT  "departure_date_Year" 
      
       ,"departure_date_Quarter" 
      
       ,"trip_SUM191793929_DateTimeOffset327175942"
FROM T_AppendCalcValue_After_Original_5f5ba4 
order by  "departure_date_Year_ORDER"  ,"departure_date_Quarter_ORDER" 
;