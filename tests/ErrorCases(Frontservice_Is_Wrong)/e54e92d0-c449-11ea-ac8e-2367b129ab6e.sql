WITH  T_Extened_6ff5c4 AS ( SELECT *
 FROM voyages
 ) 
 , T_Filter_1057b3 AS ( SELECT *
 FROM T_Extened_6ff5c4
 WHERE (((EXTRACT(Year FROM "departure_date")  = 1791 OR EXTRACT(Year FROM "departure_date")  = 1792 OR EXTRACT(Year FROM "departure_date")  = 1793 OR EXTRACT(Year FROM "departure_date")  = 1794 OR EXTRACT(Year FROM "departure_date")  = 1795))) AND (( ( "departure_date" >= sys.str_to_date('1793-02-12T00:00:00', '%Y-%m-%dT%H:%M:%S') AND "departure_date" <= sys.str_to_date('1795-01-12T23:59:59', '%Y-%m-%dT%H:%M:%S') )  And  ( "departure_date" >= sys.str_to_date('1793-01-12T00:00:00', '%Y-%m-%dT%H:%M:%S') AND "departure_date" <= sys.str_to_date('1794-10-23T23:59:59', '%Y-%m-%dT%H:%M:%S') )  And  not ( "departure_date" >= sys.str_to_date('1794-05-05T00:00:00', '%Y-%m-%dT%H:%M:%S') AND "departure_date" <= sys.str_to_date('1794-05-05T23:59:59', '%Y-%m-%dT%H:%M:%S') ))) 
 ) 
 , T_Context_006a22 AS ( SELECT EXTRACT(Year FROM "departure_date") AS "departure_date_Year" , EXTRACT(Day FROM "departure_date") AS "departure_date_Day" , EXTRACT(Month FROM "departure_date") AS "departure_date_Month" , SUM("trip") AS "trip_SUM1676374776"
 FROM T_Filter_1057b3
 GROUP BY "departure_date_Year" , "departure_date_Day" , "departure_date_Month"
 ) 
 , T_Sort_c3814b AS ( SELECT "departure_date_Year" AS "departure_date_Year"
 FROM T_Context_006a22
 GROUP BY "departure_date_Year"
 ORDER BY 1 Desc
 ) 
 , T_Sort_67b8b5 AS ( SELECT "departure_date_Day" AS "departure_date_Day"
 FROM T_Context_006a22
 GROUP BY "departure_date_Day"
 ORDER BY 1 Asc
 ) 
 , T_Sort_db6637 AS ( SELECT "departure_date_Month" AS "departure_date_Month"
 FROM T_Context_006a22
 GROUP BY "departure_date_Month"
 ORDER BY  (CASE  WHEN "departure_date_Month" =   '4'  THEN 0 WHEN "departure_date_Month" =   '3'  THEN 1 WHEN "departure_date_Month" =   '5'  THEN 2 WHEN "departure_date_Month" IS   NULL  THEN 3 WHEN "departure_date_Month" =   '9'  THEN 4 WHEN "departure_date_Month" =   '12'  THEN 5 WHEN "departure_date_Month" =   '1'  THEN 6 WHEN "departure_date_Month" =   '7'  THEN 7 WHEN "departure_date_Month" =   '8'  THEN 8 WHEN "departure_date_Month" =   '2'  THEN 9 WHEN "departure_date_Month" =   '10'  THEN 10 WHEN "departure_date_Month" =   '11'  THEN 11 WHEN "departure_date_Month" =   '6'  THEN 12 ELSE 13 END) ASC 
 ) 
 , T_Order_2e1ef6 AS ( SELECT "departure_date_Year" AS "departure_date_Year" , ROW_NUMBER()  OVER()  AS "departure_date_Year_ORDER" 
 FROM T_Sort_c3814b
 ) 
 , T_Order_3bc4b8 AS ( SELECT "departure_date_Day" AS "departure_date_Day" , ROW_NUMBER()  OVER()  AS "departure_date_Day_ORDER" 
 FROM T_Sort_67b8b5
 ) 
 , T_Order_326795 AS ( SELECT "departure_date_Month" AS "departure_date_Month" , ROW_NUMBER()  OVER()  AS "departure_date_Month_ORDER" 
 FROM T_Sort_db6637
 ) 
 , T_OrderJoin_895102 AS ( SELECT T_Context_006a22."departure_date_Year" AS "departure_date_Year" , T_Context_006a22."departure_date_Day" AS "departure_date_Day" , T_Context_006a22."departure_date_Month" AS "departure_date_Month" , T_Context_006a22."trip_SUM1676374776" AS "trip_SUM1676374776" , "departure_date_Year_ORDER" AS "departure_date_Year_ORDER" , "departure_date_Day_ORDER" AS "departure_date_Day_ORDER" , "departure_date_Month_ORDER" AS "departure_date_Month_ORDER"
 FROM T_Context_006a22 INNER JOIN T_Order_2e1ef6 ON (T_Context_006a22."departure_date_Year" = T_Order_2e1ef6."departure_date_Year" OR (T_Context_006a22."departure_date_Year" IS NULL AND  T_Order_2e1ef6."departure_date_Year" IS NULL ))  INNER JOIN T_Order_3bc4b8 ON (T_Context_006a22."departure_date_Day" = T_Order_3bc4b8."departure_date_Day" OR (T_Context_006a22."departure_date_Day" IS NULL AND  T_Order_3bc4b8."departure_date_Day" IS NULL ))  INNER JOIN T_Order_326795 ON (T_Context_006a22."departure_date_Month" = T_Order_326795."departure_date_Month" OR (T_Context_006a22."departure_date_Month" IS NULL AND  T_Order_326795."departure_date_Month" IS NULL )) 
 ) 
 , T_AppendCalcValue_Before_Original_552b57 AS ( SELECT T_OrderJoin_895102."departure_date_Year" AS "departure_date_Year" , T_OrderJoin_895102."departure_date_Day" AS "departure_date_Day" , T_OrderJoin_895102."departure_date_Month" AS "departure_date_Month" , T_OrderJoin_895102."trip_SUM1676374776" AS "trip_SUM1676374776" , T_OrderJoin_895102."departure_date_Year_ORDER" AS "departure_date_Year_ORDER" , T_OrderJoin_895102."departure_date_Day_ORDER" AS "departure_date_Day_ORDER" , T_OrderJoin_895102."departure_date_Month_ORDER" AS "departure_date_Month_ORDER"
 FROM T_OrderJoin_895102
 ) 
 , T_ContextReduce_b8839f AS ( SELECT T_AppendCalcValue_Before_Original_552b57."departure_date_Year" AS "departure_date_Year" , T_AppendCalcValue_Before_Original_552b57."departure_date_Day" AS "departure_date_Day" , T_AppendCalcValue_Before_Original_552b57."departure_date_Month" AS "departure_date_Month" , T_AppendCalcValue_Before_Original_552b57."trip_SUM1676374776" AS "trip_SUM1676374776" , T_AppendCalcValue_Before_Original_552b57."departure_date_Year_ORDER" AS "departure_date_Year_ORDER" , T_AppendCalcValue_Before_Original_552b57."departure_date_Day_ORDER" AS "departure_date_Day_ORDER" , T_AppendCalcValue_Before_Original_552b57."departure_date_Month_ORDER" AS "departure_date_Month_ORDER"
 FROM T_AppendCalcValue_Before_Original_552b57
 ) 
 , T_AppendCalcValue_After_Original_b2b644 AS ( SELECT T_ContextReduce_b8839f."departure_date_Year" AS "departure_date_Year" , T_ContextReduce_b8839f."departure_date_Day" AS "departure_date_Day" , T_ContextReduce_b8839f."departure_date_Month" AS "departure_date_Month" , T_ContextReduce_b8839f."trip_SUM1676374776" AS "trip_SUM1676374776" , T_ContextReduce_b8839f."departure_date_Year_ORDER" AS "departure_date_Year_ORDER" , T_ContextReduce_b8839f."departure_date_Day_ORDER" AS "departure_date_Day_ORDER" , T_ContextReduce_b8839f."departure_date_Month_ORDER" AS "departure_date_Month_ORDER" , SUM("trip_SUM1676374776")  OVER(PARTITION BY "departure_date_Day" ORDER BY "departure_date_Year_ORDER" ASC , "departure_date_Month_ORDER" ASC)  AS "trip_SUM1676374776_Accumulation_Running688469238" 
 FROM T_ContextReduce_b8839f
 ) 
 
SELECT  "departure_date_Year" 
       ,"departure_date_Day" 
       ,"departure_date_Month" 
       ,"trip_SUM1676374776_Accumulation_Running688469238"
FROM T_AppendCalcValue_After_Original_b2b644 
order by  "departure_date_Year_ORDER" 
       ,"departure_date_Day_ORDER" 
       ,"departure_date_Month_ORDER" 
;