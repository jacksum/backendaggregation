SELECT EXTRACT(Year FROM "cape_arrival") AS "cape_arrival_Year" , EXTRACT(Month FROM "cape_arrival") AS "cape_arrival_Month" , EXTRACT(Day FROM "cape_arrival") AS "cape_arrival_Day" , "number" AS "number" , "trip" AS "trip" , "arrival_harbour" AS "arrival_harbour"
 FROM voyages
 WHERE ( ( EXTRACT(Year FROM "cape_arrival")  >= 1192 AND EXTRACT(Year FROM "cape_arrival")  <= 1703 )  And  not ( EXTRACT(Year FROM "cape_arrival")  >= 1665 AND EXTRACT(Year FROM "cape_arrival")  <= 1667 ) And  (EXTRACT(Year FROM "cape_arrival")  <= 1670 )  And  (EXTRACT(Year FROM "cape_arrival")  < 1670 )  And  ( EXTRACT(Year FROM "cape_arrival")  >= 1652)  And  ( EXTRACT(Year FROM "cape_arrival")  > 1652)  And (EXTRACT(Year FROM "cape_arrival")  <> 1654) And (EXTRACT(Year FROM "cape_arrival")  = 1655)) 
 ORDER BY "cape_arrival" Asc , "cape_arrival" Asc , "cape_arrival" Asc
 LIMIT 200 
 OFFSET 0 ;