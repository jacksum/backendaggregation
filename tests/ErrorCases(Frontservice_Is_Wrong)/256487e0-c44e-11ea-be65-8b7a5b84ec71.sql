SELECT  EXTRACT(Year FROM "cape_arrival") AS "cape_arrival_Year" 
, EXTRACT(Month FROM "cape_arrival") AS "cape_arrival_Month" 
, EXTRACT(Day FROM "cape_arrival") AS "cape_arrival_Day" 
, "number" AS "number" 
, "trip" AS "trip" 
, "arrival_harbour" AS "arrival_harbour"
FROM voyages
WHERE (
       (EXTRACT(Year  FROM "cape_arrival") <> 1610 OR EXTRACT(Year FROM "cape_arrival") IS NOT NULL)
        AND 
        (EXTRACT(Year FROM "cape_arrival") = 1614 OR 
        EXTRACT(Year FROM "cape_arrival") = 1615 OR 
        EXTRACT(Year FROM "cape_arrival") = 1616 OR 
        EXTRACT(Year FROM "cape_arrival") = 1617 OR 
        EXTRACT(Year FROM "cape_arrival") = 1618 OR 
        EXTRACT(Year FROM "cape_arrival") = 1619)

)
ORDER BY "cape_arrival" Asc , "cape_arrival" Asc , "cape_arrival" Asc
LIMIT 200 OFFSET 0 ;