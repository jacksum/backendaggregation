SELECT  "cape_arrival"    AS "cape_arrival" 
       ,"number"          AS "number" 
       ,"trip"            AS "trip" 
       ,"arrival_harbour" AS "arrival_harbour"
FROM voyages
WHERE ( 

( EXTRACT(Year FROM "cape_arrival") >= 1192 AND EXTRACT(Year FROM "cape_arrival") <= 1703 ) 
AND not 
( EXTRACT(Year FROM "cape_arrival") >= 1665 AND EXTRACT(Year FROM "cape_arrival") <= 1667 ) 
AND (EXTRACT(Year FROM "cape_arrival") <= 1670 ) 
AND (EXTRACT(Year FROM "cape_arrival") < 1670 ) 
AND ( EXTRACT(Year FROM "cape_arrival") >= 1652) 
AND ( EXTRACT(Year FROM "cape_arrival") > 1652) 
AND (EXTRACT(Year FROM "cape_arrival") <> 1654) 
AND (EXTRACT(Year FROM "cape_arrival") = 1655)

)
ORDER BY "cape_arrival" Asc , "arrival_harbour" Asc
LIMIT 200 OFFSET 0 ;