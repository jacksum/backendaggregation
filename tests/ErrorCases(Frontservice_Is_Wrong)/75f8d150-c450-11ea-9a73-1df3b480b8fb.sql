 WITH T_Extened_800acf AS (
SELECT  *
FROM voyages ) , T_Filter_50f11d AS (
SELECT  *
FROM T_Extened_800acf
WHERE ((EXTRACT(Year 
FROM "departure_date") = 1595 OR EXTRACT(Year
FROM "departure_date") = 1598 OR EXTRACT(Year
FROM "departure_date") = 1599 OR EXTRACT(Year
FROM "departure_date") = 1600 OR EXTRACT(Year
FROM "departure_date") = 1601 OR EXTRACT(Year
FROM "departure_date") = 1602 OR EXTRACT(Year
FROM "departure_date") = 1611) AND ("arrival_harbour" = 'Engano' OR "arrival_harbour" = 'Bantam' OR "arrival_harbour" = 'Atjeh' OR "arrival_harbour" = 'Tidore' OR "arrival_harbour" = 'Ternate')) ) 

, T_Context_e20c95 AS (
SELECT  EXTRACT(Year
FROM "departure_date") AS "departure_date_Year" , "hired" AS "hired" , "arrival_harbour" AS "arrival_harbour" 
, SUM("trip") AS "trip_SUM1548274326" 
, COUNT("trip") AS "trip_COUNT1927694977"
FROM T_Filter_50f11d
GROUP BY  "departure_date_Year" 
         ,"hired" 
         ,"arrival_harbour" ) 

,T_Sort_b174dd AS (
SELECT  "departure_date_Year" AS "departure_date_Year"
FROM T_Context_e20c95
GROUP BY  "departure_date_Year"
ORDER BY 1 Asc ) 
         ,T_Sort_d6a820 AS (
SELECT  DISTINCT "hired" AS "hired"
FROM T_Extened_800acf ) , T_Sort_5ffe55 AS (
SELECT  DISTINCT "arrival_harbour" AS "arrival_harbour"
FROM T_Extened_800acf ) , T_Order_9361ea AS (
SELECT  "departure_date_Year" AS "departure_date_Year" 
       ,ROW_NUMBER() OVER()   AS "departure_date_Year_ORDER"
FROM T_Sort_b174dd ) , T_Order_dc12d2 AS (
SELECT  "hired"             AS "hired" 
       ,ROW_NUMBER() OVER() AS "hired_ORDER"
FROM T_Sort_d6a820 ) , T_Order_0dae6c AS (
SELECT  "arrival_harbour"   AS "arrival_harbour" 
       ,ROW_NUMBER() OVER() AS "arrival_harbour_ORDER"
FROM T_Sort_5ffe55 ) 
, T_OrderJoin_ff9b06 AS (
SELECT  T_Context_e20c95."departure_date_Year"  AS "departure_date_Year" 
       ,T_Context_e20c95."hired"                AS "hired" 
       ,T_Context_e20c95."arrival_harbour"      AS "arrival_harbour" 
       ,T_Context_e20c95."trip_SUM1548274326"   AS "trip_SUM1548274326" 
       ,T_Context_e20c95."trip_COUNT1927694977" AS "trip_COUNT1927694977" 
       ,"departure_date_Year_ORDER"             AS "departure_date_Year_ORDER" 
       ,"hired_ORDER"                           AS "hired_ORDER" 
       ,"arrival_harbour_ORDER"                 AS "arrival_harbour_ORDER"
FROM T_Context_e20c95
INNER JOIN T_Order_9361ea
ON (T_Context_e20c95."departure_date_Year" = T_Order_9361ea."departure_date_Year" OR (T_Context_e20c95."departure_date_Year" IS NULL AND T_Order_9361ea."departure_date_Year" IS NULL ))
INNER JOIN T_Order_dc12d2
ON (T_Context_e20c95."hired" = T_Order_dc12d2."hired" OR (T_Context_e20c95."hired" IS NULL AND T_Order_dc12d2."hired" IS NULL ))
INNER JOIN T_Order_0dae6c
ON (T_Context_e20c95."arrival_harbour" = T_Order_0dae6c."arrival_harbour" OR (T_Context_e20c95."arrival_harbour" IS NULL AND T_Order_0dae6c."arrival_harbour" IS NULL )) ) 

, T_AppendCalcValue_Before_Original_5a57da AS (
SELECT  T_OrderJoin_ff9b06."departure_date_Year"       AS "departure_date_Year" 
       ,T_OrderJoin_ff9b06."hired"                     AS "hired" 
       ,T_OrderJoin_ff9b06."arrival_harbour"           AS "arrival_harbour" 
       ,T_OrderJoin_ff9b06."trip_SUM1548274326"        AS "trip_SUM1548274326" 
       ,T_OrderJoin_ff9b06."trip_COUNT1927694977"      AS "trip_COUNT1927694977" 
       ,T_OrderJoin_ff9b06."departure_date_Year_ORDER" AS "departure_date_Year_ORDER" 
       ,T_OrderJoin_ff9b06."hired_ORDER"               AS "hired_ORDER" 
       ,T_OrderJoin_ff9b06."arrival_harbour_ORDER"     AS "arrival_harbour_ORDER"
FROM T_OrderJoin_ff9b06 ) 
, T_AppendCalcValue_Before_Avg_91ac0d AS (
SELECT  T_AppendCalcValue_Before_Original_5a57da."departure_date_Year"       AS "departure_date_Year" 
       ,T_AppendCalcValue_Before_Original_5a57da."hired"                     AS "hired" 
       ,T_AppendCalcValue_Before_Original_5a57da."arrival_harbour"           AS "arrival_harbour" 
       ,T_AppendCalcValue_Before_Original_5a57da."trip_SUM1548274326"        AS "trip_SUM1548274326" 
       ,T_AppendCalcValue_Before_Original_5a57da."trip_COUNT1927694977"      AS "trip_COUNT1927694977" 
       ,T_AppendCalcValue_Before_Original_5a57da."departure_date_Year_ORDER" AS "departure_date_Year_ORDER" 
       ,T_AppendCalcValue_Before_Original_5a57da."hired_ORDER"               AS "hired_ORDER" 
       ,T_AppendCalcValue_Before_Original_5a57da."arrival_harbour_ORDER"     AS "arrival_harbour_ORDER"
FROM T_AppendCalcValue_Before_Original_5a57da ) 
, T_ContextReduce_af2e5d AS (
SELECT  T_AppendCalcValue_Before_Avg_91ac0d."departure_date_Year"       AS "departure_date_Year" 
       ,T_AppendCalcValue_Before_Avg_91ac0d."hired"                     AS "hired" 
       ,T_AppendCalcValue_Before_Avg_91ac0d."arrival_harbour"           AS "arrival_harbour" 
       ,T_AppendCalcValue_Before_Avg_91ac0d."trip_SUM1548274326"        AS "trip_SUM1548274326" 
       ,T_AppendCalcValue_Before_Avg_91ac0d."trip_COUNT1927694977"      AS "trip_COUNT1927694977" 
       ,T_AppendCalcValue_Before_Avg_91ac0d."departure_date_Year_ORDER" AS "departure_date_Year_ORDER" 
       ,T_AppendCalcValue_Before_Avg_91ac0d."hired_ORDER"               AS "hired_ORDER" 
       ,T_AppendCalcValue_Before_Avg_91ac0d."arrival_harbour_ORDER"     AS "arrival_harbour_ORDER"
FROM T_AppendCalcValue_Before_Avg_91ac0d )
 , T_AppendCalcValue_After_Original_2a0910 AS (
SELECT  T_ContextReduce_af2e5d."departure_date_Year"                                                                       AS "departure_date_Year" 
       ,T_ContextReduce_af2e5d."hired"                                                                                     AS "hired" 
       ,T_ContextReduce_af2e5d."arrival_harbour"                                                                           AS "arrival_harbour" 
       ,T_ContextReduce_af2e5d."trip_SUM1548274326"                                                                        AS "trip_SUM1548274326" 
       ,T_ContextReduce_af2e5d."trip_COUNT1927694977"                                                                      AS "trip_COUNT1927694977" 
       ,T_ContextReduce_af2e5d."departure_date_Year_ORDER"                                                                 AS "departure_date_Year_ORDER" 
       ,T_ContextReduce_af2e5d."hired_ORDER"                                                                               AS "hired_ORDER" 
       ,T_ContextReduce_af2e5d."arrival_harbour_ORDER"                                                                     AS "arrival_harbour_ORDER" 
       ,SUM("trip_SUM1548274326") OVER(PARTITION BY "hired" ,"arrival_harbour" ORDER BY "departure_date_Year_ORDER" ASC)   AS "trip_SUM1548274326_Accumulation_Running674813030" 
       ,SUM("trip_COUNT1927694977") OVER(PARTITION BY "hired" ,"arrival_harbour" ORDER BY "departure_date_Year_ORDER" ASC) AS "trip_COUNT1927694977_Accumulation_Running642703461"
FROM T_ContextReduce_af2e5d ) 
, T_AppendCalcValue_After_Avg_0741ad AS (
SELECT  T_AppendCalcValue_After_Original_2a0910."departure_date_Year"                                                           AS "departure_date_Year" 
       ,T_AppendCalcValue_After_Original_2a0910."hired"                                                                         AS "hired" 
       ,T_AppendCalcValue_After_Original_2a0910."arrival_harbour"                                                               AS "arrival_harbour" 
       ,T_AppendCalcValue_After_Original_2a0910."trip_SUM1548274326"                                                            AS "trip_SUM1548274326" 
       ,T_AppendCalcValue_After_Original_2a0910."trip_COUNT1927694977"                                                          AS "trip_COUNT1927694977" 
       ,T_AppendCalcValue_After_Original_2a0910."departure_date_Year_ORDER"                                                     AS "departure_date_Year_ORDER" 
       ,T_AppendCalcValue_After_Original_2a0910."hired_ORDER"                                                                   AS "hired_ORDER" 
       ,T_AppendCalcValue_After_Original_2a0910."arrival_harbour_ORDER"                                                         AS "arrival_harbour_ORDER" 
       ,T_AppendCalcValue_After_Original_2a0910."trip_SUM1548274326_Accumulation_Running674813030"                              AS "trip_SUM1548274326_Accumulation_Running674813030" 
       ,T_AppendCalcValue_After_Original_2a0910."trip_COUNT1927694977_Accumulation_Running642703461"                            AS "trip_COUNT1927694977_Accumulation_Running642703461" 
       ,CAST("trip_SUM1548274326_Accumulation_Running674813030" AS DOUBLE)/"trip_COUNT1927694977_Accumulation_Running642703461" AS "trip_Avg_Accumulation980449781"
FROM T_AppendCalcValue_After_Original_2a0910 )
SELECT  "departure_date_Year" 
       ,"hired" 
       ,"arrival_harbour"  
       ,"trip_Avg_Accumulation980449781"
FROM T_AppendCalcValue_After_Avg_0741ad
order by  "departure_date_Year_ORDER"
       ,"hired_ORDER" 
       ,"arrival_harbour_ORDER"   ;