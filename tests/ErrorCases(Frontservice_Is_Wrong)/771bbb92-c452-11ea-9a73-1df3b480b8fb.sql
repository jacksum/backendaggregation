 WITH T_Extened_d2c1fa AS (
SELECT  *
FROM voyages ) 
, T_Filter_240358 AS (
SELECT  *
FROM T_Extened_d2c1fa
WHERE ((EXTRACT(Year 
FROM "departure_date") = 1595 OR EXTRACT(Year
FROM "departure_date") = 1598 OR EXTRACT(Year
FROM "departure_date") = 1599 OR EXTRACT(Year
FROM "departure_date") = 1600 OR EXTRACT(Year
FROM "departure_date") = 1601 OR EXTRACT(Year
FROM "departure_date") = 1602 OR EXTRACT(Year
FROM "departure_date") = 1611) AND ("arrival_harbour" = 'Engano' OR "arrival_harbour" = 'Atjeh' OR "arrival_harbour" = 'Ternate' OR "arrival_harbour" = 'Ceylon')) ) 

, T_Context_5b0650 AS (
SELECT  "arrival_harbour" AS "arrival_harbour" 
       ,EXTRACT(Quarter
FROM "departure_date") AS "departure_date_Quarter" , EXTRACT(Year
FROM "departure_date") AS "departure_date_Year" 
, MAX("trip") AS "trip_MAX395499030"
FROM T_Filter_240358
GROUP BY  "arrival_harbour" 
         ,"departure_date_Quarter" 
         ,"departure_date_Year" ) 

,T_Sort_5f1c8f AS (
SELECT  DISTINCT "arrival_harbour" AS "arrival_harbour"
FROM T_Extened_d2c1fa ) 
, T_Sort_238c36 AS (
SELECT  "departure_date_Quarter" AS "departure_date_Quarter"
FROM T_Context_5b0650
GROUP BY  "departure_date_Quarter"
ORDER BY 1 Asc ) 

 ,T_Sort_420a52 AS (
SELECT  "departure_date_Year" AS "departure_date_Year"
FROM T_Context_5b0650
GROUP BY  "departure_date_Year"
ORDER BY 1 Asc ) 
 
 ,T_Order_56b041 AS (
SELECT  "arrival_harbour"   AS "arrival_harbour" 
       ,ROW_NUMBER() OVER() AS "arrival_harbour_ORDER"
FROM T_Sort_5f1c8f )
 , T_Order_fa619e AS (
SELECT  "departure_date_Quarter" AS "departure_date_Quarter" 
       ,ROW_NUMBER() OVER()      AS "departure_date_Quarter_ORDER"
FROM T_Sort_238c36 ) 
, T_Order_777fc0 AS (
SELECT  "departure_date_Year" AS "departure_date_Year" 
       ,ROW_NUMBER() OVER()   AS "departure_date_Year_ORDER"
FROM T_Sort_420a52 )

, T_OrderJoin_342f4a AS (
SELECT  T_Context_5b0650."arrival_harbour"        AS "arrival_harbour" 
       ,T_Context_5b0650."departure_date_Quarter" AS "departure_date_Quarter" 
       ,T_Context_5b0650."departure_date_Year"    AS "departure_date_Year" 
       ,T_Context_5b0650."trip_MAX395499030"      AS "trip_MAX395499030" 
       ,"arrival_harbour_ORDER"                   AS "arrival_harbour_ORDER" 
       ,"departure_date_Quarter_ORDER"            AS "departure_date_Quarter_ORDER" 
       ,"departure_date_Year_ORDER"               AS "departure_date_Year_ORDER"
FROM T_Context_5b0650
INNER JOIN T_Order_56b041
ON (T_Context_5b0650."arrival_harbour" = T_Order_56b041."arrival_harbour" OR (T_Context_5b0650."arrival_harbour" IS NULL AND T_Order_56b041."arrival_harbour" IS NULL ))
INNER JOIN T_Order_fa619e
ON (T_Context_5b0650."departure_date_Quarter" = T_Order_fa619e."departure_date_Quarter" OR (T_Context_5b0650."departure_date_Quarter" IS NULL AND T_Order_fa619e."departure_date_Quarter" IS NULL ))
INNER JOIN T_Order_777fc0
ON (T_Context_5b0650."departure_date_Year" = T_Order_777fc0."departure_date_Year" OR (T_Context_5b0650."departure_date_Year" IS NULL AND T_Order_777fc0."departure_date_Year" IS NULL )) ) 

, T_AppendCalcValue_Before_Original_ec5209 AS (
SELECT  T_OrderJoin_342f4a."arrival_harbour"              AS "arrival_harbour" 
       ,T_OrderJoin_342f4a."departure_date_Quarter"       AS "departure_date_Quarter" 
       ,T_OrderJoin_342f4a."departure_date_Year"          AS "departure_date_Year" 
       ,T_OrderJoin_342f4a."trip_MAX395499030"            AS "trip_MAX395499030" 
       ,T_OrderJoin_342f4a."arrival_harbour_ORDER"        AS "arrival_harbour_ORDER" 
       ,T_OrderJoin_342f4a."departure_date_Quarter_ORDER" AS "departure_date_Quarter_ORDER" 
       ,T_OrderJoin_342f4a."departure_date_Year_ORDER"    AS "departure_date_Year_ORDER"
FROM T_OrderJoin_342f4a ) 

, T_ContextReduce_caa697 AS (
SELECT  T_AppendCalcValue_Before_Original_ec5209."arrival_harbour"              AS "arrival_harbour" 
       ,T_AppendCalcValue_Before_Original_ec5209."departure_date_Quarter"       AS "departure_date_Quarter" 
       ,T_AppendCalcValue_Before_Original_ec5209."departure_date_Year"          AS "departure_date_Year" 
       ,T_AppendCalcValue_Before_Original_ec5209."trip_MAX395499030"            AS "trip_MAX395499030" 
       ,T_AppendCalcValue_Before_Original_ec5209."arrival_harbour_ORDER"        AS "arrival_harbour_ORDER" 
       ,T_AppendCalcValue_Before_Original_ec5209."departure_date_Quarter_ORDER" AS "departure_date_Quarter_ORDER" 
       ,T_AppendCalcValue_Before_Original_ec5209."departure_date_Year_ORDER"    AS "departure_date_Year_ORDER"
FROM T_AppendCalcValue_Before_Original_ec5209 ) 

, T_AppendCalcValue_After_Original_a2964a AS (
SELECT  T_ContextReduce_caa697."arrival_harbour"                                               AS "arrival_harbour" 
       ,T_ContextReduce_caa697."departure_date_Quarter"                                        AS "departure_date_Quarter" 
       ,T_ContextReduce_caa697."departure_date_Year"                                           AS "departure_date_Year" 
       ,T_ContextReduce_caa697."trip_MAX395499030"                                             AS "trip_MAX395499030" 
       ,T_ContextReduce_caa697."arrival_harbour_ORDER"                                         AS "arrival_harbour_ORDER" 
       ,T_ContextReduce_caa697."departure_date_Quarter_ORDER"                                  AS "departure_date_Quarter_ORDER" 
       ,T_ContextReduce_caa697."departure_date_Year_ORDER"                                     AS "departure_date_Year_ORDER" 
       ,ROW_NUMBER() OVER(PARTITION BY "departure_date_Year" ORDER BY "trip_MAX395499030" Asc) AS "trip_MAX834324198_Rank866495534"
FROM T_ContextReduce_caa697 ) 

, T_ContextHandle_03d48a AS (
SELECT  T_AppendCalcValue_After_Original_a2964a."arrival_harbour"                                  AS "arrival_harbour" 
       ,T_AppendCalcValue_After_Original_a2964a."departure_date_Quarter"                           AS "departure_date_Quarter" 
       ,T_AppendCalcValue_After_Original_a2964a."departure_date_Year"                              AS "departure_date_Year" 
       ,T_AppendCalcValue_After_Original_a2964a."trip_MAX395499030"                                AS "trip_MAX395499030" 
       ,T_AppendCalcValue_After_Original_a2964a."arrival_harbour_ORDER"                            AS "arrival_harbour_ORDER" 
       ,T_AppendCalcValue_After_Original_a2964a."departure_date_Quarter_ORDER"                     AS "departure_date_Quarter_ORDER" 
       ,T_AppendCalcValue_After_Original_a2964a."departure_date_Year_ORDER"                        AS "departure_date_Year_ORDER" 
       ,T_AppendCalcValue_After_Original_a2964a."trip_MAX834324198_Rank866495534"                  AS "trip_MAX834324198_Rank866495534" 
       ,CASE WHEN "trip_MAX395499030" IS NULL THEN NULL ELSE "trip_MAX834324198_Rank866495534" END AS "trip_Max_Rank2127295326"
FROM T_AppendCalcValue_After_Original_a2964a )
SELECT  "arrival_harbour" 
       ,"departure_date_Quarter" 
       ,"departure_date_Year"  
       ,"trip_Max_Rank2127295326"
FROM T_ContextHandle_03d48a 
order by 
        "arrival_harbour_ORDER" 
       ,"departure_date_Quarter_ORDER"
       ,"departure_date_Year_ORDER"  ;