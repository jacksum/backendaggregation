 WITH T_Extened_ac5beb AS (
SELECT  * 
       ,"departure_date" - INTERVAL '3' Year AS "departure_date_OFFSET1764797789"
FROM voyages ) , T_Filter_cd1a03 AS (
SELECT  *
FROM T_Extened_ac5beb
WHERE ((EXTRACT(Year 
FROM "departure_date") = 1598 OR EXTRACT(Year
FROM "departure_date") = 1599 OR EXTRACT(Year
FROM "departure_date") = 1600 OR EXTRACT(Year
FROM "departure_date") = 1601 OR EXTRACT(Year
FROM "departure_date") = 1602 OR EXTRACT(Year
FROM "departure_date") = 1603) AND ( "departure_date" >= sys.str_to_date('1599-01-01T00:00:00', '%Y-%m-%dT%H:%M:%S') AND "departure_date" <= sys.str_to_date('1602-12-31T23:59:59', '%Y-%m-%dT%H:%M:%S') ) ) ) , T_FilterOffset_493965_departure_date_OFFSET1764797789 AS (
SELECT  *
FROM T_Extened_ac5beb
WHERE ((EXTRACT(Year 
FROM "departure_date_OFFSET1764797789") = 1598 OR EXTRACT(Year
FROM "departure_date_OFFSET1764797789") = 1599 OR EXTRACT(Year
FROM "departure_date_OFFSET1764797789") = 1600 OR EXTRACT(Year
FROM "departure_date_OFFSET1764797789") = 1601 OR EXTRACT(Year
FROM "departure_date_OFFSET1764797789") = 1602 OR EXTRACT(Year
FROM "departure_date_OFFSET1764797789") = 1603) AND ( "departure_date_OFFSET1764797789" >= sys.str_to_date('1599-01-01T00:00:00', '%Y-%m-%dT%H:%M:%S') AND "departure_date_OFFSET1764797789" <= sys.str_to_date('1602-12-31T23:59:59', '%Y-%m-%dT%H:%M:%S') ) ) ) , T_Context_a5799c AS (
SELECT  EXTRACT(Year
FROM "departure_date") AS "departure_date_Year" , SUM("trip") AS "trip_SUM1652514939"
FROM T_Filter_cd1a03
GROUP BY  "departure_date_Year" ) 
         ,T_ContextOffset_4c04d8 AS (
SELECT  EXTRACT(Year
FROM "departure_date_OFFSET1764797789") AS "departure_date_OFFSET1764797789_Year" , SUM("trip") AS "trip_SUM379496378"
FROM T_FilterOffset_493965_departure_date_OFFSET1764797789
GROUP BY  "departure_date_OFFSET1764797789_Year" ) 
         ,T_Sort_de6363 AS (
SELECT  "departure_date_Year" AS "departure_date_Year"
FROM T_Context_a5799c
GROUP BY  "departure_date_Year"
ORDER BY 1 Desc ) 
         ,T_Order_f582f2 AS (
SELECT  "departure_date_Year" AS "departure_date_Year" 
       ,ROW_NUMBER() OVER()   AS "departure_date_Year_ORDER"
FROM T_Sort_de6363 ) , T_OrderJoin_3fdfe2 AS (
SELECT  T_Context_a5799c."departure_date_Year" AS "departure_date_Year" 
       ,T_Context_a5799c."trip_SUM1652514939"  AS "trip_SUM1652514939" 
       ,"departure_date_Year_ORDER"            AS "departure_date_Year_ORDER"
FROM T_Context_a5799c
INNER JOIN T_Order_f582f2
ON (T_Context_a5799c."departure_date_Year" = T_Order_f582f2."departure_date_Year" OR (T_Context_a5799c."departure_date_Year" IS NULL AND T_Order_f582f2."departure_date_Year" IS NULL )) ) , T_OffsetLeftJoin_2e0390 AS (
SELECT  T_OrderJoin_3fdfe2."departure_date_Year"                                                             AS "departure_date_Year" 
       ,T_OrderJoin_3fdfe2."trip_SUM1652514939"                                                              AS "trip_SUM1652514939" 
       ,T_OrderJoin_3fdfe2."departure_date_Year_ORDER"                                                       AS "departure_date_Year_ORDER" 
       ,CAST((T_OrderJoin_3fdfe2."trip_SUM1652514939"-T_ContextOffset_4c04d8."trip_SUM379496378") AS DOUBLE) AS "trip_SUM379496378_DateTimeOffset2105278487"
FROM T_OrderJoin_3fdfe2
LEFT JOIN T_ContextOffset_4c04d8
ON ((T_OrderJoin_3fdfe2."departure_date_Year" = T_ContextOffset_4c04d8."departure_date_OFFSET1764797789_Year" OR (T_OrderJoin_3fdfe2."departure_date_Year" IS NULL AND T_ContextOffset_4c04d8."departure_date_OFFSET1764797789_Year" IS NULL))) ) , T_AppendCalcValue_Before_Original_216001 AS (
SELECT  T_OffsetLeftJoin_2e0390."departure_date_Year"                        AS "departure_date_Year" 
       ,T_OffsetLeftJoin_2e0390."trip_SUM1652514939"                         AS "trip_SUM1652514939" 
       ,T_OffsetLeftJoin_2e0390."departure_date_Year_ORDER"                  AS "departure_date_Year_ORDER" 
       ,T_OffsetLeftJoin_2e0390."trip_SUM379496378_DateTimeOffset2105278487" AS "trip_SUM379496378_DateTimeOffset2105278487"
FROM T_OffsetLeftJoin_2e0390 ) , T_ContextReduce_2f1ea3 AS (
SELECT  T_AppendCalcValue_Before_Original_216001."departure_date_Year"                        AS "departure_date_Year" 
       ,T_AppendCalcValue_Before_Original_216001."trip_SUM1652514939"                         AS "trip_SUM1652514939" 
       ,T_AppendCalcValue_Before_Original_216001."departure_date_Year_ORDER"                  AS "departure_date_Year_ORDER" 
       ,T_AppendCalcValue_Before_Original_216001."trip_SUM379496378_DateTimeOffset2105278487" AS "trip_SUM379496378_DateTimeOffset2105278487"
FROM T_AppendCalcValue_Before_Original_216001 ) , T_AppendCalcValue_After_Original_8f9471 AS (
SELECT  T_ContextReduce_2f1ea3."departure_date_Year"                        AS "departure_date_Year" 
       ,T_ContextReduce_2f1ea3."trip_SUM1652514939"                         AS "trip_SUM1652514939" 
       ,T_ContextReduce_2f1ea3."departure_date_Year_ORDER"                  AS "departure_date_Year_ORDER" 
       ,T_ContextReduce_2f1ea3."trip_SUM379496378_DateTimeOffset2105278487" AS "trip_SUM379496378_DateTimeOffset2105278487"
FROM T_ContextReduce_2f1ea3 )
SELECT  "departure_date_Year" 
       
       ,"trip_SUM379496378_DateTimeOffset2105278487"
FROM T_AppendCalcValue_After_Original_8f9471 
order by "departure_date_Year_ORDER" ;