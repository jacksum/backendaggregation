 WITH T_Extened_27071e AS (
SELECT  * 
       ,"departure_date" + INTERVAL '3' Month AS "departure_date_OFFSET1732398689"
FROM voyages ) 

, T_Filter_8cf093 AS (
SELECT  *
FROM T_Extened_27071e
WHERE ((EXTRACT(Year 
FROM "departure_date") = 1598 OR EXTRACT(Year
FROM "departure_date") = 1599 OR EXTRACT(Year
FROM "departure_date") = 1600 OR EXTRACT(Year
FROM "departure_date") = 1601 OR EXTRACT(Year
FROM "departure_date") = 1602 OR EXTRACT(Year
FROM "departure_date") = 1603)) ) 

, T_FilterOffset_947b4e_departure_date_OFFSET1732398689 AS (
SELECT  *
FROM T_Extened_27071e
WHERE ((EXTRACT(Year 
FROM "departure_date_OFFSET1732398689") = 1598 OR EXTRACT(Year
FROM "departure_date_OFFSET1732398689") = 1599 OR EXTRACT(Year
FROM "departure_date_OFFSET1732398689") = 1600 OR EXTRACT(Year
FROM "departure_date_OFFSET1732398689") = 1601 OR EXTRACT(Year
FROM "departure_date_OFFSET1732398689") = 1602 OR EXTRACT(Year
FROM "departure_date_OFFSET1732398689") = 1603)) ) 


, T_Context_a5a716 AS (
SELECT  EXTRACT(Year
FROM "departure_date") AS "departure_date_Year" , EXTRACT(Quarter
FROM "departure_date") AS "departure_date_Quarter" , SUM("trip") AS "trip_SUM80498006"
FROM T_Filter_8cf093
GROUP BY  "departure_date_Year" 
         ,"departure_date_Quarter" ) 


,T_ContextOffset_b62d93 AS (
SELECT  
EXTRACT(Year FROM "departure_date_OFFSET1732398689") AS "departure_date_OFFSET1732398689_Year" ,
EXTRACT(Quarter FROM "departure_date_OFFSET1732398689") AS "departure_date_OFFSET1732398689_Quarter" , 
SUM("trip") AS "trip_SUM226911033"
FROM T_FilterOffset_947b4e_departure_date_OFFSET1732398689
GROUP BY  "departure_date_OFFSET1732398689_Year" ,"departure_date_OFFSET1732398689_Quarter" ) 


,T_Sort_67b821 AS (
SELECT  "departure_date_Year" AS "departure_date_Year"
FROM T_Context_a5a716
GROUP BY  "departure_date_Year"
ORDER BY 1 Asc ) 


,T_Sort_f39ce7 AS (
SELECT  "departure_date_Quarter" AS "departure_date_Quarter"
FROM T_Context_a5a716
GROUP BY  "departure_date_Quarter"
ORDER BY 1 Asc ) 


,T_Order_91ea76 AS (
SELECT  "departure_date_Year" AS "departure_date_Year" 
       ,ROW_NUMBER() OVER()   AS "departure_date_Year_ORDER"
FROM T_Sort_67b821 ) 

, T_Order_8ce754 AS (
SELECT  "departure_date_Quarter" AS "departure_date_Quarter" 
       ,ROW_NUMBER() OVER()      AS "departure_date_Quarter_ORDER"
FROM T_Sort_f39ce7 )

 , T_OrderJoin_9494de AS (
SELECT  T_Context_a5a716."departure_date_Year"    AS "departure_date_Year" 
       ,T_Context_a5a716."departure_date_Quarter" AS "departure_date_Quarter" 
       ,T_Context_a5a716."trip_SUM80498006"       AS "trip_SUM80498006" 
       ,"departure_date_Year_ORDER"               AS "departure_date_Year_ORDER" 
       ,"departure_date_Quarter_ORDER"            AS "departure_date_Quarter_ORDER"
FROM T_Context_a5a716
INNER JOIN T_Order_91ea76
ON (T_Context_a5a716."departure_date_Year" = T_Order_91ea76."departure_date_Year" OR (T_Context_a5a716."departure_date_Year" IS NULL AND T_Order_91ea76."departure_date_Year" IS NULL ))
INNER JOIN T_Order_8ce754
ON (T_Context_a5a716."departure_date_Quarter" = T_Order_8ce754."departure_date_Quarter" OR (T_Context_a5a716."departure_date_Quarter" IS NULL AND T_Order_8ce754."departure_date_Quarter" IS NULL )) ) 


, T_OffsetLeftJoin_2dd12c AS (
SELECT  T_OrderJoin_9494de."departure_date_Year"          AS "departure_date_Year" 
       ,T_OrderJoin_9494de."departure_date_Quarter"       AS "departure_date_Quarter" 
       ,T_OrderJoin_9494de."trip_SUM80498006"             AS "trip_SUM80498006" 
       ,T_OrderJoin_9494de."departure_date_Year_ORDER"    AS "departure_date_Year_ORDER" 
       ,T_OrderJoin_9494de."departure_date_Quarter_ORDER" AS "departure_date_Quarter_ORDER" 
       ,T_ContextOffset_b62d93."trip_SUM226911033"        AS "trip_SUM226911033_DateTimeOffset328686707"
FROM T_OrderJoin_9494de
LEFT JOIN T_ContextOffset_b62d93
ON ((T_OrderJoin_9494de."departure_date_Year" = T_ContextOffset_b62d93."departure_date_OFFSET1732398689_Year"
 OR (T_OrderJoin_9494de."departure_date_Year" IS NULL AND T_ContextOffset_b62d93."departure_date_OFFSET1732398689_Year" IS NULL)
 ) 
 AND (T_OrderJoin_9494de."departure_date_Quarter" = T_ContextOffset_b62d93."departure_date_OFFSET1732398689_Quarter"
  OR (T_OrderJoin_9494de."departure_date_Quarter" IS NULL AND T_ContextOffset_b62d93."departure_date_OFFSET1732398689_Quarter" IS NULL)
  
  )) ) 


, T_AppendCalcValue_Before_Original_12c3bf AS (
SELECT  T_OffsetLeftJoin_2dd12c."departure_date_Year"                       AS "departure_date_Year" 
       ,T_OffsetLeftJoin_2dd12c."departure_date_Quarter"                    AS "departure_date_Quarter" 
       ,T_OffsetLeftJoin_2dd12c."trip_SUM80498006"                          AS "trip_SUM80498006" 
       ,T_OffsetLeftJoin_2dd12c."departure_date_Year_ORDER"                 AS "departure_date_Year_ORDER" 
       ,T_OffsetLeftJoin_2dd12c."departure_date_Quarter_ORDER"              AS "departure_date_Quarter_ORDER" 
       ,T_OffsetLeftJoin_2dd12c."trip_SUM226911033_DateTimeOffset328686707" AS "trip_SUM226911033_DateTimeOffset328686707"
FROM T_OffsetLeftJoin_2dd12c ) 

, T_ContextReduce_9da95b AS (
SELECT  T_AppendCalcValue_Before_Original_12c3bf."departure_date_Year"                       AS "departure_date_Year" 
       ,T_AppendCalcValue_Before_Original_12c3bf."departure_date_Quarter"                    AS "departure_date_Quarter" 
       ,T_AppendCalcValue_Before_Original_12c3bf."trip_SUM80498006"                          AS "trip_SUM80498006" 
       ,T_AppendCalcValue_Before_Original_12c3bf."departure_date_Year_ORDER"                 AS "departure_date_Year_ORDER" 
       ,T_AppendCalcValue_Before_Original_12c3bf."departure_date_Quarter_ORDER"              AS "departure_date_Quarter_ORDER" 
       ,T_AppendCalcValue_Before_Original_12c3bf."trip_SUM226911033_DateTimeOffset328686707" AS "trip_SUM226911033_DateTimeOffset328686707"
FROM T_AppendCalcValue_Before_Original_12c3bf ) 

, T_AppendCalcValue_After_Original_9303b6 AS (
SELECT  T_ContextReduce_9da95b."departure_date_Year"                       AS "departure_date_Year" 
       ,T_ContextReduce_9da95b."departure_date_Quarter"                    AS "departure_date_Quarter" 
       ,T_ContextReduce_9da95b."trip_SUM80498006"                          AS "trip_SUM80498006" 
       ,T_ContextReduce_9da95b."departure_date_Year_ORDER"                 AS "departure_date_Year_ORDER" 
       ,T_ContextReduce_9da95b."departure_date_Quarter_ORDER"              AS "departure_date_Quarter_ORDER" 
       ,T_ContextReduce_9da95b."trip_SUM226911033_DateTimeOffset328686707" AS "trip_SUM226911033_DateTimeOffset328686707"
FROM T_ContextReduce_9da95b )



SELECT  "departure_date_Year" 
       ,"departure_date_Quarter" 
       ,"trip_SUM80498006"   
       ,"trip_SUM226911033_DateTimeOffset328686707"
FROM T_AppendCalcValue_After_Original_9303b6 
order by  "departure_date_Year_ORDER" 
       ,"departure_date_Quarter_ORDER" ;