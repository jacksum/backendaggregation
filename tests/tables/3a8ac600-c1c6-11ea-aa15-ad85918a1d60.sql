-- COPY
SELECT
  boatname
FROM
  voyages
ORDER BY
  tonnage DESC
LIMIT
  200
OFFSET
  0;
-- INTO './tests/testData.csv' ON CLIENT
-- USING delimiters ',';