-- COPY
SELECT
  tonnage,
  boatname
FROM
  voyages
ORDER BY
  tonnage DESC
LIMIT
  200
OFFSET
  200;
-- INTO './tests/testData.csv' ON CLIENT
-- USING delimiters ',';