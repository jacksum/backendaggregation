COPY
SELECT
  type_of_boat
FROM
  voyages
WHERE
  master IS NOT NULL
  AND
  (type_of_boat ILIKE '%oo%' OR type_of_boat ILIKE '%ja%')
ORDER BY (
  CASE type_of_boat
    WHEN 'bootje'
    THEN 0
    WHEN 'galjoot'
    THEN 1
    WHEN 'jacht'
    THEN 2
    ELSE 3
  END
) ASC
LIMIT
  100
OFFSET
  0
INTO './tests/testData.csv' ON CLIENT
USING delimiters ',';