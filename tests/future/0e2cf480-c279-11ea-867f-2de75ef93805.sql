-- COPY
-- explain
WITH
  data_view AS (
    SELECT
      *
    FROM
      voyages
    WHERE
      master IS NOT NULL
  ),
  context AS (
    SELECT
      master
    FROM
      data_view
    GROUP BY
      master
  ),
  first_of_boatname AS (
    SELECT
      DISTINCT master,
      sys.first_value(boatname) OVER (
        PARTITION BY
          master
      ) AS first_of_boatname
    FROM
      data_view
  ),
  master_sort AS (
    SELECT
      DISTINCT master,
      sys.first_value(tonnage) OVER (
        PARTITION BY
          master
      ) AS first_of_tonnage
    FROM
      data_view
    ORDER BY
      2 DESC
  ),
  master_order AS (
    SELECT
      master,
      ROW_NUMBER() OVER () AS master_order
    FROM
      master_sort
  ),
  group_for_master AS (
    SELECT
      master,
      SUM(tonnage) AS sum_of_tonnage
    FROM
      data_view
    GROUP BY
      master
  ),
  rank_for_master AS (
    SELECT
      master,
      DENSE_RANK() OVER (
        ORDER BY
          sum_of_tonnage DESC
      ) AS rank_for_master
    FROM
      group_for_master
  ),
  with_all AS (
    SELECT
      context.*,
      master_order.master_order,
      first_of_boatname.first_of_boatname
    FROM
      context
        INNER JOIN first_of_boatname ON context.master = first_of_boatname.master OR (context.master IS NULL AND first_of_boatname.master IS NULL)
        INNER JOIN master_order ON context.master = master_order.master OR (context.master IS NULL AND master_order.master IS NULL)
       INNER JOIN rank_for_master ON context.master = rank_for_master.master OR (context.master IS NULL AND rank_for_master.master IS NULL)
    WHERE
      rank_for_master <= 5
  )
SELECT
  master,
  first_of_boatname
FROM
  with_all
ORDER BY
  master_order ASC;
-- INTO './tests/testData.csv' ON CLIENT
-- USING delimiters ',';