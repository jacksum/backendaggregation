-- COPY
WITH
  data_view AS (
    SELECT
      *
    FROM
      voyages
    WHERE
      EXTRACT(YEAR FROM arrival_date) IN (
        '1651',
        '1652',
        '1653',
        '1654',
        '1655',
        '1656',
        '1657',
        '1658',
        '1659',
        '1660',
        '1661',
        '1662',
        '1663',
        '1664',
        '1665',
        '1666',
        '1667',
        '1668',
        '1669',
        '1670'
      )
  ),
  context AS (
    SELECT
      EXTRACT(YEAR FROM arrival_date) AS arrival_date_year,
      SUM(tonnage) AS sum_of_tonnage
    FROM
      data_view
    GROUP BY
      arrival_date_year
  ),
  arrival_date_year_sort AS (
    SELECT
      EXTRACT(YEAR FROM arrival_date) AS arrival_date_year,
      SUM(tonnage) AS sum_of_tonnage
    FROM
      data_view
    GROUP BY
      arrival_date_year
    ORDER BY
      2 DESC
  ),
  arrival_date_year_order AS (
    SELECT
      arrival_date_year,
      ROW_NUMBER() OVER () AS arrival_date_year_order
    FROM
      arrival_date_year_sort
  ),
  group_for_arrival_date_year AS (
    SELECT
      EXTRACT(YEAR FROM arrival_date) AS arrival_date_year,
      SUM(tonnage) AS sum_of_tonnage
    FROM
      data_view
    GROUP BY
      arrival_date_year
  ),
  rank_on_sum_of_tonnage AS (
    SELECT
      arrival_date_year,
      ROW_NUMBER() OVER (
        ORDER BY
          sum_of_tonnage DESC
      ) AS rank_on_sum_of_tonnage
    FROM
      group_for_arrival_date_year
  ),
  with_calc AS (
    SELECT
      context.*,
      arrival_date_year_order.arrival_date_year_order,
      rank_on_sum_of_tonnage.rank_on_sum_of_tonnage,
      ROW_NUMBER() OVER (
        ORDER BY
          sum_of_tonnage DESC
      ) AS rank_filter_for_arrival_date_year
    FROM
      context
        INNER JOIN arrival_date_year_order ON context.arrival_date_year = arrival_date_year_order.arrival_date_year OR (context.arrival_date_year IS NULL AND arrival_date_year_order.arrival_date_year IS NULL)
        INNER JOIN rank_on_sum_of_tonnage ON context.arrival_date_year = rank_on_sum_of_tonnage.arrival_date_year OR (context.arrival_date_year IS NULL AND rank_on_sum_of_tonnage.arrival_date_year IS NULL)
  ),
  with_all AS (
    SELECT
      *
    FROM
      with_calc
    WHERE
      rank_filter_for_arrival_date_year >= 3
      AND
      rank_filter_for_arrival_date_year <= 10
  ),
  data_view_with_filter AS (
    SELECT
      data_view.*
    FROM
      data_view
        INNER JOIN with_all ON (EXTRACT(YEAR FROM data_view.arrival_date) = with_all.arrival_date_year) OR (EXTRACT(YEAR FROM data_view.arrival_date) IS NULL AND with_all.arrival_date_year IS NULL)
  ),
  group_for_arrival_date_year2 AS (
    SELECT
      EXTRACT(YEAR FROM arrival_date) AS arrival_date_year,
      SUM(tonnage) AS sum_of_tonnage
    FROM
      data_view_with_filter
    GROUP BY
      arrival_date_year
  ),
  rank_on_sum_of_tonnage2 AS (
    SELECT
      arrival_date_year,
      ROW_NUMBER() OVER (
        ORDER BY
          sum_of_tonnage DESC
      ) AS rank_on_sum_of_tonnage2
    FROM
      group_for_arrival_date_year2
  ),
  with_all2 AS (
    SELECT
      with_all.*,
      rank_on_sum_of_tonnage2.rank_on_sum_of_tonnage2
    FROM
      with_all
        INNER JOIN rank_on_sum_of_tonnage2 ON with_all.arrival_date_year = rank_on_sum_of_tonnage2.arrival_date_year OR (with_all.arrival_date_year IS NULL AND rank_on_sum_of_tonnage2.arrival_date_year IS NULL)
  )
SELECT
  arrival_date_year,
  sum_of_tonnage,
  rank_on_sum_of_tonnage2,
  rank_on_sum_of_tonnage
FROM
  with_all2
ORDER BY
  arrival_date_year_order;
-- INTO './tests/testData.csv' ON CLIENT
-- USING delimiters ',';