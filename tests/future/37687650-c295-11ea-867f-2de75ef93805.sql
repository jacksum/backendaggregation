-- COPY
WITH
  data_view AS (
    SELECT
      *
    FROM
      voyages
    WHERE
      master IS NOT NULL
  ),
  context AS (
    SELECT
      master,
      COUNT(DISTINCT(boatname)) AS dst_of_boatname
    FROM
      data_view
    GROUP BY
      master
  ),
  master_sort AS (
    SELECT
      DISTINCT master
    FROM
      voyages
  ),
  master_order AS (
    SELECT
      master,
      ROW_NUMBER() OVER () AS master_order
    FROM
      master_sort
  ),
  group_for_master AS (
    SELECT
      master,
      SUM(tonnage) AS sum_of_tonnage
    FROM
      data_view
    GROUP BY
      master
  ),
  rank_for_master AS (
    SELECT
      master,
      DENSE_RANK() OVER (
        ORDER BY
          sum_of_tonnage DESC
      ) AS rank_for_master
    FROM
      group_for_master
  ),
  with_all_before AS (
    SELECT
      context.*,
      master_order.master_order,
      CAST(dst_of_boatname AS DOUBLE) / SUM(dst_of_boatname) OVER () AS proportion_dst_of_boatname_before
    FROM
      context
        INNER JOIN master_order ON context.master = master_order.master OR (context.master IS NULL AND master_order.master IS NULL)
  ),
  with_all AS (
    SELECT
      with_all_before.*,
      CAST(dst_of_boatname AS DOUBLE) / SUM(dst_of_boatname) OVER () AS proportion_dst_of_boatname_after
    FROM
      with_all_before
        INNER JOIN rank_for_master ON with_all_before.master = rank_for_master.master OR (with_all_before.master IS NULL AND rank_for_master.master IS NULL)
    WHERE
      rank_for_master <= 5
  )
SELECT
  master,
  proportion_dst_of_boatname_after,
  proportion_dst_of_boatname_before
FROM
  with_all
ORDER BY
  master_order ASC;
-- INTO './tests/testData.csv' ON CLIENT
-- USING delimiters ',';