-- COPY
WITH
  extend AS (
    SELECT
      *,
      departure_date + INTERVAL '1' YEAR AS departure_date_offset
    FROM
      voyages
  ),
  context AS (
    SELECT
      SUM(tonnage) AS sum_of_tonnage
    FROM
      extend
    WHERE
      departure_date >= sys.str_to_date('1700-01-01T00:00:00', '%Y-%m-%dT%H:%M:%S')
      AND
      departure_date <= sys.str_to_date('1700-12-31T23:59:59', '%Y-%m-%dT%H:%M:%S')
  ),
  context_offset AS (
    SELECT
      SUM(tonnage) AS sum_of_tonnage
    FROM
      extend
    WHERE
      departure_date_offset >= sys.str_to_date('1700-01-01T00:00:00', '%Y-%m-%dT%H:%M:%S')
      AND
      departure_date_offset <= sys.str_to_date('1700-12-31T23:59:59', '%Y-%m-%dT%H:%M:%S')
  ),
  with_all AS (
    SELECT
      context.*,
      context.sum_of_tonnage - context_offset.sum_of_tonnage AS yoy_sum_of_tonnage
    FROM
      context CROSS JOIN context_offset
  )
SELECT
  yoy_sum_of_tonnage
FROM
  with_all;
-- INTO './tests/testData.csv' ON CLIENT
-- USING delimiters ',';