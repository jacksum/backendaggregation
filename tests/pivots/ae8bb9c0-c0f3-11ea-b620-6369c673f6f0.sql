-- COPY
WITH
  data_view AS (
    SELECT
      *
    FROM
      voyages
    WHERE
      chamber IS NOT NULL
      AND
      type_of_boat IS NOT NULL
      AND
      type_of_boat NOT ILIKE '%?%'
  ),
  context AS (
    SELECT
      chamber,
      type_of_boat,
      SUM(tonnage) AS sum_of_tonnage
    FROM
      data_view
    GROUP BY
      type_of_boat,
      chamber
  ),
  chamber_sort AS (
    SELECT
      DISTINCT chamber
    FROM
      voyages
  ),
  chamber_order AS (
    SELECT
      chamber,
      ROW_NUMBER() OVER () AS chamber_order
    FROM
      chamber_sort
  ),
  type_of_boat_sort AS (
    SELECT
      DISTINCT type_of_boat
    FROM
      voyages
  ),
  type_of_boat_order AS (
    SELECT
      type_of_boat,
      ROW_NUMBER() OVER () AS type_of_boat_order
    FROM
      type_of_boat_sort
  ),
  filtered_context AS (
    SELECT
      *,
      ROW_NUMBER() OVER (
        PARTITION BY
          type_of_boat
        ORDER BY
          sum_of_tonnage DESC
      ) AS partiton_filter
    FROM
      context
  ),
  with_all AS (
    SELECT
      filtered_context.*,
      type_of_boat_order.type_of_boat_order,
      chamber_order.chamber_order,
      ROW_NUMBER() OVER (
        ORDER BY
          sum_of_tonnage DESC
      ) AS rank_on_sum_of_tonnage,
      sys.ms_trunc(CAST(sum_of_tonnage AS DOUBLE) / SUM(sum_of_tonnage) OVER ( -- to third decimal places
        PARTITION BY
          filtered_context.type_of_boat
      ), 3) AS proportion_sum_of_tonnage
    FROM
      filtered_context
        INNER JOIN chamber_order ON filtered_context.chamber = chamber_order.chamber OR (filtered_context.chamber IS NULL AND chamber_order.chamber IS NULL)
        INNER JOIN type_of_boat_order ON filtered_context.type_of_boat = type_of_boat_order.type_of_boat OR (filtered_context.type_of_boat IS NULL AND type_of_boat_order.type_of_boat IS NULL)
    WHERE
      partiton_filter <= 3
  ),
  with_all2 AS (
    SELECT
      *, 
      CASE
        WHEN sum_of_tonnage IS NULL
        THEN NULL
        ELSE rank_on_sum_of_tonnage
      END AS rank_on_sum_of_tonnage2
    FROM
      with_all
  )
SELECT
  chamber,
  type_of_boat,
  sum_of_tonnage,
  rank_on_sum_of_tonnage2,
  proportion_sum_of_tonnage
FROM
  with_all2
ORDER BY
  chamber_order ASC,
  type_of_boat_order ASC;
-- INTO './tests/testData.csv' ON CLIENT
-- USING delimiters ',';