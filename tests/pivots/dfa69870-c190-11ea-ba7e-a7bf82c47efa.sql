-- COPY
WITH
  data_view AS (
    SELECT
      *,
      arrival_date + INTERVAL '1' year AS arrival_date_offset
    FROM
      voyages
    WHERE
      arrival_date IS NOT NULL
  ),
  context AS (
    SELECT
      EXTRACT(YEAR FROM arrival_date) AS arrival_date_year,
      SUM(tonnage) AS sum_of_tonnage
    FROM
      data_view
    GROUP BY
      arrival_date_year
  ),
  context_offset AS (
    SELECT
      EXTRACT(YEAR FROM arrival_date_offset) AS arrival_date_year_offset,
      SUM(tonnage) AS sum_of_tonnage
    FROM
      data_view
    GROUP BY
      arrival_date_year_offset
  ),
  arrival_date_year_sort AS (
    SELECT
      DISTINCT arrival_date_year
    FROM
      context
    ORDER BY
      1 ASC
  ),
  arrival_date_year_order AS (
    SELECT
      arrival_date_year,
      ROW_NUMBER() OVER () AS arrival_date_year_order
    FROM
      arrival_date_year_sort
  ),
  with_all AS (
    SELECT
      context.*,
      arrival_date_year_order.arrival_date_year_order,
      sys.ms_trunc(CAST((context.sum_of_tonnage - context_offset.sum_of_tonnage) AS DOUBLE) / context_offset.sum_of_tonnage, 3) AS yoy_sum_of_tonnage
    FROM
      context
        INNER JOIN arrival_date_year_order ON context.arrival_date_year = arrival_date_year_order.arrival_date_year OR (context.arrival_date_year IS NULL AND arrival_date_year_order.arrival_date_year IS NULL)
        LEFT JOIN context_offset ON context.arrival_date_year = context_offset.arrival_date_year_offset OR (context.arrival_date_year IS NULL AND context_offset.arrival_date_year_offset IS NULL)
  )
SELECT
  arrival_date_year,
  sum_of_tonnage,
  yoy_sum_of_tonnage
FROM
  with_all
ORDER BY
  arrival_date_year_order ASC;
-- INTO './tests/testData.csv' ON CLIENT
-- USING delimiters ',';