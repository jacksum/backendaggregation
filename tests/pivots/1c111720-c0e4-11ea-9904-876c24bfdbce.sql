-- COPY
WITH
  data_view AS (
    SELECT
      *
    FROM
      voyages
    WHERE
      EXTRACT(YEAR FROM arrival_date) = 1647
  ),
  context AS (
    SELECT
      EXTRACT(YEAR FROM arrival_date) AS arrival_date_year,
      EXTRACT(MONTH FROM arrival_date) AS arrival_date_month,
      SUM(tonnage) AS sum_of_tonnage
    FROM
      data_view
    GROUP BY
      arrival_date_year,
      arrival_date_month
  ),
  arrival_date_month_sort AS (
    SELECT
      DISTINCT arrival_date_month
    FROM
      context
    ORDER BY
      1 ASC
  ),
  arrival_date_month_order AS (
    SELECT
      arrival_date_month,
      ROW_NUMBER() OVER () AS arrival_date_month_order
    FROM
      arrival_date_month_sort
  ),
  arrival_date_year_sort AS (
    SELECT
      DISTINCT arrival_date_year
    FROM
      context
    ORDER BY
      1 ASC
  ),
  arrival_date_year_order AS (
    SELECT
      arrival_date_year,
      ROW_NUMBER() OVER () AS arrival_date_year_order
    FROM
      arrival_date_year_sort
  ),
  with_all AS (
    SELECT
      context.*,
      arrival_date_year_order.arrival_date_year_order,
      arrival_date_month_order.arrival_date_month_order,
      SUM(sum_of_tonnage) OVER (
        ORDER BY
          context.arrival_date_year ASC,
          context.arrival_date_month ASC
      ) AS running_sum_on_sum_of_tonnage,
      SUM(sum_of_tonnage) OVER (
        ORDER BY
          context.arrival_date_year ASC,
          context.arrival_date_month ASC
        ROWS
          BETWEEN 1 PRECEDING AND 1 FOLLOWING
      ) AS moving_sum_on_sum_of_tonnage
    FROM
      context
        INNER JOIN arrival_date_year_order ON context.arrival_date_year = arrival_date_year_order.arrival_date_year OR (context.arrival_date_year IS NULL AND arrival_date_year_order.arrival_date_year IS NULL)
        INNER JOIN arrival_date_month_order ON context.arrival_date_month = arrival_date_month_order.arrival_date_month OR (context.arrival_date_month IS NULL AND arrival_date_month_order.arrival_date_month IS NULL)
  )
SELECT
  arrival_date_year,
  arrival_date_month,
  sum_of_tonnage,
  running_sum_on_sum_of_tonnage,
  moving_sum_on_sum_of_tonnage
FROM
  with_all
ORDER BY
  arrival_date_year_order ASC,
  arrival_date_month_order ASC;
-- INTO './tests/testData.csv' ON CLIENT
-- USING delimiters ',';