COPY
WITH
  context AS (
    SELECT
      type_of_boat,
      SUM(tonnage) AS sum_of_tonnage
    FROM
      voyages
    WHERE
      type_of_boat IS NOT NULL
    GROUP BY
      type_of_boat
  ),
  type_of_boat_sort AS (
    SELECT
      sum_of_tonnage,
      type_of_boat
    FROM
      context
    ORDER BY
      1 DESC
  ),
  type_of_boat_order AS (
    SELECT
      type_of_boat,
      ROW_NUMBER() OVER () AS type_of_boat_order
    FROM
      type_of_boat_sort
  ),
  with_all AS (
    SELECT
      context.*,
      type_of_boat_order.type_of_boat_order
    FROM
      context
        INNER JOIN type_of_boat_order ON context.type_of_boat = type_of_boat_order.type_of_boat OR (context.type_of_boat IS NULL AND type_of_boat_order.type_of_boat IS NULL)
    WHERE
      sum_of_tonnage > 1000

  )
SELECT
  type_of_boat,
  sum_of_tonnage
FROM
  with_all
ORDER BY
  type_of_boat_order ASC
INTO './tests/testData.csv' ON CLIENT
USING delimiters ',';