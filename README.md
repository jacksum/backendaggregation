## Purpose
Explore the capability of MonetDB for data analysis

## Get Started
### Mac OS X
1. Install dependencies `brew install coreutils`
1. Install monetDB `brew install monetdb`
1. Init a monetDB instance `./scripts/init.sh`
1. Query on monetDB `./scripts/query.sh querySample/querySQL/PivotSample.sql`

### Windows
1. Not supported yet

### Interactive Terminal
1. Connect monetDB: `mclient -u voc -d voc` 
1. Input password: `voc`
1. Play with any SQL

## MonetDB
1. [Tutorial](https://www.monetdb.org/Documentation/Tutorial)
1. [Built-in SQL types](https://www.monetdb.org/Documentation/SQLReference/DataTypes/BuiltinSQLTypes)
1. [Window functions](https://www.monetdb.org/Documentation/SQLReference/DataManipulation/WindowFunctions)

## Document
1. [Wiki](https://wiki.grapecity.io/pages/viewpage.action?pageId=1133702955)

## Todo
### Loss of function
1. Support `ModifiedCompetition` (1,3,3,4)
1. Moving value, includes non-exist entry, should be `NULL`
1. Support changing week strategy: ISO week or US week.

### Improvement
1. Support `FIRST` function (returns the first field value of the given column)
1. Support `EXCLUDE CURRENT ROW`

## Unit Test
1. use same precision for decimal number [3]
1. keep the same fields order: Dimension[], Value[], CalcValue[]
1. `ORDER BY` order fields and remove order fields.

## Not Supported Features
1. [Recursive CTE](https://www.monetdb.org/bugzilla/show_bug.cgi?id=6702)
1. [EXCLUDING CURRENT ROW](https://www.monetdb.org/blog/extended_sql_window_functions)

## Usage Questions
1. [No ordinary select query in procedure](https://www.monetdb.org/pipermail/users-list/2012-January/005340.html)
1. Use Function rather than Procedure to get ordinary query work.

## Differences
1. RankCalcValue: `null` for `null` value
1. Rank in Percentage: (rank - 1)/(count - 1) <---> rank/count
1. Same values in unique rank make result different
1. sort fields but many rows are equal to each other

## Data Reduction
### Reduce rows count to `N`
1. `N` is specified by user  
1. Positions
    1. Filter
    1. Context (Values & Dimensions)
    1. `Limit N`: same to previous implementation
    1. Sort Orders (`Join`)
    1. CalcValue (`Join`)
    1. CalcFilter
    1. CalcValue (`Join`)
    1. Select All 
    1. `Limit N`: All is well without any performance improvement
### Sampling
1. Pick some entries from each dimension fields (similar to PowerBI)

## Performance
1. `Join` two big `CTE` will consuming all disk IO
    1. Change CTE to temp physical table.
    1. Reduce Context CTE to `N` rows (before any join).