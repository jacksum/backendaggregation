import { Dimension, Value, CalcValue, Filter, CalcFilter, LogicalTree } from './common';

export type PivotModel = {
  id: Id,
  dimensions: Dimension[],
  values?: Value[],
  calcValues?: CalcValue[]
  filters?: LogicalTree<Filter>,
  calcFilters?: CalcFilter[],
};

export type RawModel = {
  id: Id,
  dimensions: Dimension[], // exclude order by value
  options: {
    capacity: PositiveInteger,
    index: NonNegativeInteger,
  }
  filters?: LogicalTree<Filter>,
}

export type AggregationModel = {
  pivots: PivotModel[], // slicer and pivot chart
  tables: RawModel[], // dataTable
  filters?: LogicalTree<Filter>, // global filter
}
