import * as enums from './enum';

export type Column = { // dataset field identifier
  columnId: Name,
  subColumnType: enums.SubColumnType,
}

export type Value = { // aggregated field
  aggregationType: enums.AggregationMethod,
  column: Column,
}
export type ComputationalValue = { // aggregated field
  aggregationType: enums.ComputationalMethod,
  column: Column,
}

//#region calculation
type CalcValueOptions = {
  afterCalcFilter: boolean, // control the order between CalcValue and CalcFilter
}
type DivideBy = {
  type: enums.DivideType,
  columns: Column[],
}

type IdentityValue = {
  type: enums.CalculationType.Identity,
  value: ComputationalValue,
  divideBy: DivideBy, // partitioned identity value equals to base value
  options: CalcValueOptions,
}

type AggregationValue = {
  type: enums.CalculationType.Aggregation,
  value: ComputationalValue,
  divideBy: DivideBy,
  options: {
    aggregationType: enums.ComputationalMethod,
  } & CalcValueOptions,
}

type AccumulationOptions = {
  accumulationType: enums.AccumulationType, // along.method
  include?: boolean,
  next?: number,
  previous?: number,
} & CalcValueOptions;

type AccumulationValue = {
  type: enums.CalculationType.Accumulation,
  value: ComputationalValue,
  divideBy: DivideBy,
  options: AccumulationOptions,
};

type RankOptions = {
  usePercentage: boolean, // rank.usePercentage
  order: enums.Order, // along.options.sortOrder
  treatDuplicateAs: enums.TreatDuplicateAs,
}
type RankValue = {
  type: enums.CalculationType.Rank,
  value: ComputationalValue,
  divideBy: DivideBy,
  options: RankOptions & CalcValueOptions,
}

type PercentValue = {
  type: enums.CalculationType.Percentage,
  value: ComputationalValue,
  divideBy: DivideBy,
  options: CalcValueOptions,
}

type DateOffsetValue = {
  type: enums.CalculationType.DateTimeOffset,
  value: ComputationalValue,
  options: {
    offset: number, // along.offset
    subdate: Column, // along.dateName + along.subType
    method: enums.DateCalculationType, // along.method//每个type的区别
  } & CalcValueOptions,
}
//#endregion
export type CalcValue = IdentityValue | AggregationValue | AccumulationValue | RankValue | PercentValue | DateOffsetValue;

//#region filter
type FilterByRange = {
  type: enums.FilterByType.Greater | enums.FilterByType.Range | enums.FilterByType.Less,
  range: {
    min: number | DateTime,
    max: number | DateTime,
    minIncluded: boolean,
    maxIncluded: boolean,
  }
  exclude: boolean,
}

type Entry = {
  value: number | string | DateTime,
  children?: Entry[],
}
type FilterByExact = {
  type: enums.FilterByType.Exact,
  range: {
    items: Entry[],
  }
  exclude: boolean,
}

type FilterByWildcard = {
  type: enums.FilterByType.MatchWord | enums.FilterByType.StartWith | enums.FilterByType.EndWith | enums.FilterByType.Contain,
  range: {
    pattern: string,
    caseSensitive: boolean,
  }
  exclude: boolean,
}

type FilterBy = FilterByExact | FilterByWildcard | FilterByRange;
//#endregion
export type Filter = {
  columns: Column[],
  filterBy: FilterBy,
}

//#region conditional filter
type RankValueFilter = Omit<RankValue, 'options'> & {
  filterBy: FilterBy,
  options: RankOptions,
}

type IdentityValueFilter = Omit<IdentityValue, 'options'> & {
  filterBy: FilterBy, // filter
}
//#endregion
export type CalcFilter = RankValueFilter | IdentityValueFilter;

//#region sort
type SortByAuto = {
  type: enums.SortType.Auto,
  order: enums.Order,
}
type SortByValue = {
  type: enums.SortType.Value,
  order: enums.Order,
  value: Value,
}
type SortByColumn = { // should be removed after adding FIRST aggregation method
  type: enums.SortType.Column,
  order: enums.Order,
  column: Column,
}
type SortByManual = {
  type: enums.SortType.Manual,
  manual:(number | string | null)[],
}

type OrderBy = SortByAuto | SortByValue | SortByManual;
//#endregion
export type Dimension = { // grouping field
  column: Column,
  orderBy?: OrderBy,
}

export type LogicalTree<T> = {
  type: enums.LogicalType,
  items: (T | LogicalTree<T>)[],
}