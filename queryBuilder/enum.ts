export enum SubColumnType {
  None = 0x0000,
  Year = 0x0001,
  Quarter = 0x0002,
  Month = 0x0004,
  Week = 0x0008,
  Day = 0x0010,
  Hour = 0x0020,
  Minute = 0x0040,
  Second = 0x0080,
  Millisecond = 0x0100,
  DatetimePart = 0x01ff,
}

export enum SpecialMethod {
  Fst = 'fst', // first
}
export enum ComputationalMethod {
  Sum = 'sum',
  Cnt = 'cnt',
  Max = 'max',
  Min = 'min',
  Avg = 'avg',
  Dst = 'dst', // distinct count
}

export type AggregationMethod = SpecialMethod | ComputationalMethod;

export enum AccumulationType { // without Dst
  Sum = 'sum',
  Cnt = 'cnt',
  Max = 'max',
  Min = 'min',
  Avg = 'avg',
}

export enum CalculationType {
  Accumulation = 'accumulation',
  Aggregation = 'aggregation',
  Percentage = 'percentage',
  Rank = 'rank',
  DateTimeOffset = 'dateTimeOffset',
  Identity = 'identity',
}
export enum DivideType {
  Partition = 'partition',
  Group = 'group',
}

export enum FilterByType {
  Exact = 10,
  Range = 20,
  Greater = 21,
  Less = 22,
  StartWith = 30,
  EndWith = 31,
  Contain = 32,
  MatchWord = 33,
}

export enum Order {
  Asc = 'ascending',
  Desc = 'descending',
}

export enum DateCalculationType {
  Value = 'value', // => prevValue
  Ratio = 'ratio', // => curValue / prevValue
  Difference = 'difference', // => curValue - prevValue
  PercentDifference = 'percentDifference', // (curValue - prevValue) / prevValue
};

export enum SortType {
  Auto = 'auto',
  Value = 'value',
  Manual = 'manual',
  Column = 'column',
}

export enum TreatDuplicateAs {
  Competition = 1, // 1,2,2,4
  ModifiedCompetition = 2, // 1,3,3,4
  Dense = 3, // 1,2,2,3
  Unique = 4, // 1,2,3,4
}

export enum LogicalType {
  And = 'and',
  Or = 'or',
}