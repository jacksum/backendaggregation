-- COPY
WITH
  context AS (
    SELECT
      type_of_boat,
      SUM(tonnage) AS sum_of_tonnage
    FROM
      voyages
    GROUP BY
      type_of_boat
  ),
  with_all AS (
    SELECT
      *,
      CAST(sum_of_tonnage AS DOUBLE) / SUM(sum_of_tonnage) OVER () AS proportion_sum_of_tonnage
    FROM
      context
  )
SELECT
  type_of_boat,
   proportion_sum_of_tonnage
FROM
  with_all;
-- INTO './querySample/queryResults/CalcValue/Percent.csv' ON CLIENT
-- USING delimiters ',';
