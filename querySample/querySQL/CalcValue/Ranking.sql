-- COPY
WITH
  context AS (
    SELECT
      type_of_boat,
      AVG(tonnage) AS avg_of_tonnage
    FROM
      voyages
    GROUP BY
      type_of_boat
  ),
  with_calculation AS (
    SELECT
      *,
      ROW_NUMBER() OVER (
        ORDER BY
          avg_of_tonnage DESC --能否直接把上面t1的sql的avg替换到这里来？---可以到时候试试。暂时先认为不支持。
      ) AS rank_avg_of_tonnage
    FROM
      context
  ),
  with_all AS (
    SELECT
      *,
      CASE
        WHEN avg_of_tonnage IS NULL
        THEN NULL
        ELSE rank_avg_of_tonnage
      END AS rank_avg_of_tonnage2
    FROM
      with_calculation
  )
SELECT
  type_of_boat,
  rank_avg_of_tonnage2
FROM
  with_all;
-- INTO './querySample/queryResults/CalcValue/Ranking.csv' ON CLIENT
-- USING delimiters ',';