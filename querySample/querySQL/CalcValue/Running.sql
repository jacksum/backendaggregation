-- EXPLAIN
-- COPY
WITH
  t AS (
    SELECT
      type_of_boat,
      SUM(tonnage) AS sum_of_tonnage,
      EXTRACT(YEAR FROM arrival_date) AS arrival_date_year
    FROM
      voyages
    WHERE
      EXTRACT(YEAR FROM arrival_date) > 1700
      AND
      EXTRACT(YEAR FROM arrival_date) < 1705
      AND
      type_of_boat IN ('fluit', 'pinas')
      AND
      tonnage IS NOT NULL
    GROUP BY
      arrival_date_year,
      type_of_boat
  ),
  t2 AS (
  SELECT
    *,
    SUM(sum_of_tonnage) OVER (
      PARTITION BY
        type_of_boat
      ORDER BY
        arrival_date_year ASC --这个必须得加
    ) AS running_sum_on_sum_of_tonnage
  FROM
    t
  )
SELECT
  type_of_boat,
  arrival_date_year,
  sum_of_tonnage,
  running_sum_on_sum_of_tonnage
FROM
  t2
ORDER BY
  arrival_date_year ASC,
  type_of_boat ASC;
-- INTO './querySample/queryResults/CalcValue/Running.csv' ON CLIENT
-- USING delimiters ',';
