-- COPY
WITH
  t AS (
    SELECT
      CAST(tonnage AS DOUBLE) AS tonnage,
      arrival_date,
      arrival_date + INTERVAL '1' year AS arrival_date_offset
    FROM
      voyages
  ),
  t2 AS (
    SELECT
      SUM(tonnage) AS sum_of_tonnage
    FROM
      t
    WHERE
      EXTRACT(YEAR FROM arrival_date) = 1788
  ),
  t3 AS (
    SELECT
      SUM(tonnage) AS sum_of_tonnage_offset
    FROM
      t
    WHERE
      EXTRACT(YEAR FROM arrival_date_offset) = 1788
  )
SELECT
  (t2.sum_of_tonnage - t3.sum_of_tonnage_offset) / t3.sum_of_tonnage_offset AS sum_of_tonnage_yoy
FROM
  t2 CROSS JOIN t3;
-- INTO './querySample/queryResults/CalcValue/DateCalcWithoutDimension.csv' ON CLIENT
-- USING delimiters ',';