-- EXPLAIN
-- COPY
WITH
  arrival_date_year_sort AS (
    SELECT
      EXTRACT(YEAR FROM arrival_date) AS arrival_date_year
    FROM
      voyages
    WHERE
      EXTRACT(YEAR FROM arrival_date) > 1700 --参数如何传入进来的，实际上是应该从filter中传入过来的。
      AND
      EXTRACT(YEAR FROM arrival_date) < 1705 --参数如何传入进来的，实际上是应该从filter中传入过来的。
    GROUP BY
      arrival_date_year
    ORDER BY
      1 ASC --为啥写1？简写， 意思是select中第一个元素。
  ),
  arrival_date_year_order AS ( --这个表在上面的model中没有体现啊？ ranking，为啥需要这个表。---隐式的order， 意味着， 有个orderby的model就得需要有这个order表类的信息（row_number）
    SELECT
      *,
      ROW_NUMBER() OVER () AS arrival_date_year_order
    FROM
      arrival_date_year_sort
  ),
  context AS (
    SELECT
      type_of_boat,
      EXTRACT(YEAR FROM arrival_date) AS arrival_date_year,
      AVG(tonnage) AS avg_of_tonnage,
      CAST(SUM(tonnage) AS DOUBLE) AS sum_of_tonnage,
      COUNT(*) AS cnt
    FROM
      voyages
    WHERE
      EXTRACT(YEAR FROM arrival_date) > 1700
      AND
      EXTRACT(YEAR FROM arrival_date) < 1705
      AND
      type_of_boat IN ('fluit', 'pinas')
      AND
      tonnage IS NOT NULL --原则上是通过filter指定。
    GROUP BY
      arrival_date_year,
      type_of_boat
  ),
  with_running AS (
    SELECT
      *,
      SUM(sum_of_tonnage) OVER (
        PARTITION BY
          type_of_boat
        ORDER BY
          arrival_date_year ASC  
      ) AS running_sum_on_sum_of_tonnage,
      SUM(cnt) OVER (
        PARTITION BY
          type_of_boat
        ORDER BY
          arrival_date_year ASC  
      ) AS running_sum_on__cnt
    FROM
      context
  )
SELECT
  type_of_boat,
  with_running.arrival_date_year,
  arrival_date_year_order,
  avg_of_tonnage,
  (running_sum_on_sum_of_tonnage / running_sum_on__cnt) AS running_avg_on_avg_of_tonnage
FROM
  with_running
    INNER JOIN arrival_date_year_order ON with_running.arrival_date_year = arrival_date_year_order.arrival_date_year
ORDER BY
  arrival_date_year_order ASC;
-- INTO './querySample/queryResults/CalcValue/RunningAvgOnAvg.csv' ON CLIENT
-- USING delimiters ',';
