-- COPY
WITH
  context AS (
    SELECT
      type_of_boat,
      master
    FROM
      voyages
    GROUP BY
      type_of_boat,
      master
  ),
  group_rank AS (
    SELECT
      SUM(tonnage) AS sum_of_tonnage,
      master
    FROM
      voyages
    GROUP BY
      master
  ),
  rank_on_tonnage AS (
    SELECT
      sum_of_tonnage AS group_rank_target,
      ROW_NUMBER() OVER(
        ORDER BY
          sum_of_tonnage DESC
      ) AS rank_on_tonnage,
      master
    FROM
      group_rank   
  ),
  with_calculations AS (
    SELECT
      context.*,
      rank_on_tonnage.rank_on_tonnage,
      rank_on_tonnage.group_rank_target
    FROM
      context
        INNER JOIN rank_on_tonnage ON context.master = rank_on_tonnage.master OR (context.master IS NULL AND rank_on_tonnage.master IS NULL)
  ),
  with_all AS (
    SELECT
      *,
      CASE
        WHEN group_rank_target IS NULL
        THEN NULL
        ELSE rank_on_tonnage
      END AS rank_on_tonnage2
    FROM
      with_calculations
  )
SELECT
  master,
  type_of_boat,
  rank_on_tonnage2
FROM
  with_all;
-- INTO './querySample/queryResults/CalcValue/GroupRanking.csv' ON CLIENT
-- USING delimiters ',';