-- COPY
WITH
  t AS (
    SELECT
      EXTRACT(MONTH FROM arrival_date) AS arrival_date_month,
      SUM(tonnage) AS sum_of_tonnage
    FROM
      voyages
    WHERE
      arrival_date IS NOT NULL
      AND
      EXTRACT(YEAR FROM arrival_date) = 1723
    GROUP BY
      arrival_date_month
  ),
  t2 AS (
    SELECT
      *,
      SUM(sum_of_tonnage) OVER (
        ORDER BY
          arrival_date_month ASC
        ROWS
          BETWEEN 1 PRECEDING AND 1 FOLLOWING
      ) AS moving_sum_on_sum_of_tonnage
    FROM
      t
  )
SELECT
  arrival_date_month,
  sum_of_tonnage,
  moving_sum_on_sum_of_tonnage
FROM
  t2
ORDER BY
  arrival_date_month ASC;
-- INTO './querySample/queryResults/CalcValue/MovingIncludeSelf.csv' ON CLIENT
-- USING delimiters ',';