{
  dimensions: [{
    column: {
      columnId: 'type_of_boat',
      subColumnType: 0
    }
  }, {
    column: {
      columnId: 'master',
      subColumnType: 0
    }
  }],
  calcValues: [{
    type: 'identity',
    value: {
      aggregationType: 'sum',
      column: {
        columnId: 'tonnage',
        subColumnType: 0
      }
    },
    divideBy: {
      type: 'group',
      columns: [{
        columnId: 'master',
        subColumnType: 0
      }]
    },
    options: {
      afterCalcFilter: false,
    }
  }]
};

-- COPY
-- explain
WITH
  context AS (
    SELECT
      type_of_boat,
      master
    FROM
      voyages
    GROUP BY
      type_of_boat,
      master
  ),
  master_sort AS (
    SELECT
      DISTINCT master
    FROM
      context
  ),
  master_order AS (
    SELECT
      master,
      ROW_NUMBER() OVER () AS master_order
    FROM
      master_sort
  ),
  type_of_boat_sort AS (
    SELECT
      DISTINCT type_of_boat
    FROM
      context
  ),
  type_of_boat_order AS (
    SELECT
      type_of_boat,
      ROW_NUMBER() OVER () AS type_of_boat_order
    FROM
      type_of_boat_sort
  ),
  group_filter_tonnage AS (
    SELECT
      SUM(tonnage) AS sum_of_tonnage,
      master
    FROM
      voyages
    GROUP BY
      master
  ),
  with_all AS (
    SELECT
      context.*,
      group_filter_tonnage.sum_of_tonnage,
      master_order.master_order,
      type_of_boat_order.type_of_boat_order
    FROM
      context
        INNER JOIN group_filter_tonnage ON context.master = group_filter_tonnage.master OR (context.master IS NULL AND group_filter_tonnage.master IS NULL)
        INNER JOIN master_order ON context.master = master_order.master OR (context.master IS NULL AND master_order.master IS NULL)
        INNER JOIN type_of_boat_order ON context.type_of_boat = type_of_boat_order.type_of_boat OR (context.type_of_boat IS NULL AND type_of_boat_order.type_of_boat IS NULL)
  )
SELECT
  type_of_boat,
  type_of_boat_order,
  master,
  master_order,
  sum_of_tonnage
FROM
  with_all;
-- INTO './querySample/queryResults/CalcValue/Accounting.csv' ON CLIENT
-- USING delimiters ',';