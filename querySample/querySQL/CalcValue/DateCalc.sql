-- COPY
WITH
  data_view AS (
    SELECT
      *,
      arrival_date + INTERVAL '1' year AS arrival_date_offset --offset是-1，这块为啥是1？
    FROM
      voyages
  ),
  context AS (
    SELECT
      SUM(tonnage) AS sum_of_tonnage,
      EXTRACT(MONTH FROM arrival_date) AS arrival_date_month
    FROM
      data_view
    WHERE
      EXTRACT(YEAR FROM arrival_date) = 1788
    GROUP BY
      arrival_date_month
  ),
  arrival_date_month_sort AS (
    SELECT
      arrival_date_month
    FROM
      context
    ORDER BY
      1 ASC
  ),
  arrival_date_month_order AS (
    SELECT
      arrival_date_month,
      ROW_NUMBER() OVER () AS arrival_date_month_order
    FROM
      arrival_date_month_sort
  ),
  context_offset AS (
    SELECT
      SUM(tonnage) AS sum_of_tonnage_offset,
      EXTRACT(MONTH FROM arrival_date_offset) AS arrival_date_month
    FROM
      data_view
    WHERE
      EXTRACT(YEAR FROM arrival_date_offset) = 1788
    GROUP BY
      arrival_date_month
  ),
  with_all AS (
    SELECT
      context.*,
      arrival_date_month_order.arrival_date_month_order,
      CAST((context.sum_of_tonnage - context_offset.sum_of_tonnage_offset) AS DOUBLE) / context_offset.sum_of_tonnage_offset AS yoy_sum_of_tonnage
    FROM
      context
        LEFT JOIN context_offset ON context.arrival_date_month = context_offset.arrival_date_month OR (context.arrival_date_month IS NULL AND context_offset.arrival_date_month IS NULL)
        INNER JOIN arrival_date_month_order ON context.arrival_date_month = arrival_date_month_order.arrival_date_month OR (context.arrival_date_month IS NULL AND arrival_date_month_order.arrival_date_month IS NULL)
  )
SELECT
  arrival_date_month,
  arrival_date_month_order,
  yoy_sum_of_tonnage
FROM
  with_all;
-- INTO './querySample/queryResults/CalcValue/DateCalc.csv' ON CLIENT
-- USING delimiters ',';