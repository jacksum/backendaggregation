{
  dimensions: [
    {
      column: {
        columnId: 'type_of_boat',
        subColumnType: 0
      }
    },
    {
      column: {
        columnId: 'arrival_date',
        subColumnType: 1
      }
    },
  ],
  values: [{
    aggregationType: 'sum',
    column: {
      columnId: 'tonnage',
      subColumnType: 0
    }
  }],
  calcValues: [{
    type: 'aggregation',
    value: {
      aggregationType: 'sum',
      column: {
        columnId: 'tonnage',
        subColumnType: 0
      }
    },
    divideBy: {
      type: 'partition',
      columns: [{
        columnId: 'type_of_boat',
        subColumnType: 1
      }]
    },
    options: {
      afterCalcFilter: false,
      aggregationType: 'sum',
    }
  }],
  filters: {
    type: 'and',
    items: [{
      columns: [{
        columnId: 'arrival_date',
        subColumnType: 1
      }],
      filterBy: {
        type: 20,
        range: {
          max: 1700,
          min: 1705,
          maxIncluded: false,
          minIncluded: false
        },
        exclude: false
      }
    }, {
      columns: [{
        columnId: 'type_of_boat',
        subColumnType: 0
      }],
      filterBy: {
        type: 10,
        range: {
          items: [{
            value: 'fluit'
          }, {
            value: 'pinas'
          }],
        },
        exclude: false
      }
    }, {
      columns: [{
        columnId: 'tonnage',
        subColumnType: 0
      }],
      filterBy: {
        type: 10,
        range: {
          items: [{
            value: null
          }],
        },
        exclude: false
      }
    }]
  }
};

-- EXPLAIN
-- COPY
WITH
  context AS (
    SELECT
      type_of_boat,
      SUM(tonnage) AS sum_of_tonnage,
      EXTRACT(YEAR FROM arrival_date) AS arrival_date_year
    FROM
      voyages
    WHERE
      EXTRACT(YEAR FROM arrival_date) > 1700
      AND
      EXTRACT(YEAR FROM arrival_date) < 1705
      AND
      type_of_boat IN ('fluit', 'pinas')
      AND
      tonnage IS NOT NULL
    GROUP BY
      arrival_date_year,
      type_of_boat
  ),
  arrival_date_year_sort AS (
    SELECT
      DISTINCT arrival_date_year
    FROM
      context
  ),
  arrival_date_year_order AS (
    SELECT
      arrival_date_year,
      ROW_NUMBER() OVER () AS arrival_date_year_order
    FROM
      arrival_date_year_sort
  ),
  type_of_boat_sort AS (
    SELECT
      DISTINCT type_of_boat
    FROM
      context
  ),
  type_of_boat_order AS (
    SELECT
      type_of_boat,
      ROW_NUMBER() OVER () AS type_of_boat_order
    FROM
      type_of_boat_sort
  ),  
  with_all AS (
  SELECT
    context.*,
    SUM(sum_of_tonnage) OVER (
      PARTITION BY
        context.type_of_boat
    ) AS sum_on_sum_of_tonnage,
    type_of_boat_order.type_of_boat_order,
    arrival_date_year_order.arrival_date_year_order
  FROM
    context
      INNER JOIN type_of_boat_order ON context.type_of_boat = type_of_boat_order.type_of_boat OR (context.type_of_boat IS NULL AND type_of_boat_order.type_of_boat IS NULL)
      INNER JOIN arrival_date_year_order ON context.arrival_date_year = arrival_date_year_order.arrival_date_year OR (context.arrival_date_year IS NULL AND arrival_date_year_order.arrival_date_year IS NULL)
  )
SELECT
  type_of_boat,
  type_of_boat_order,
  arrival_date_year,
  arrival_date_year_order,
  sum_of_tonnage,
  sum_on_sum_of_tonnage
FROM
  with_all;
-- INTO './querySample/queryResults/CalcValue/Aggregation.csv' ON CLIENT
-- USING delimiters ',';
