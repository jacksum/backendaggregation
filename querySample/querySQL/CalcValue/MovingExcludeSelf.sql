-- EXPLAIN
-- COPY
WITH
  t AS (
    SELECT
      EXTRACT(MONTH FROM arrival_date) AS arrival_month,
      SUM(tonnage) AS sum_of_tonnage
    FROM
      voyages
    WHERE
      arrival_date IS NOT NULL
      AND
      EXTRACT(YEAR FROM arrival_date) = 1723
    GROUP BY
      arrival_month
  ),
  t2 AS (
    SELECT
      arrival_month,
      SUM(sum_of_tonnage) OVER (
        ORDER BY
          arrival_month ASC
        ROWS
          BETWEEN 1 PRECEDING AND 1 PRECEDING --2个preceding作用？
      ) AS moving_sum_on_sum_of_tonnage
    FROM
      t
    UNION ALL
    SELECT
      arrival_month,
      SUM(sum_of_tonnage) OVER (
        ORDER BY
          arrival_month ASC
        ROWS
          BETWEEN 1 FOLLOWING AND 1 FOLLOWING
      ) AS moving_sum_on_sum_of_tonnage
    FROM
      t
  ),
  t3 AS (
    SELECT
      arrival_month,
      SUM(moving_sum_on_sum_of_tonnage) AS moving_sum_on_sum_of_tonnage
    FROM
      t2
    GROUP BY
      arrival_month
  )
SELECT
  t.arrival_month,
  t.sum_of_tonnage,
  t3.moving_sum_on_sum_of_tonnage
FROM
  t INNER JOIN t3 ON t.arrival_month = t3.arrival_month
ORDER BY
  t.arrival_month;
-- INTO './querySample/queryResults/CalcValue/MovingExcludeSelf.csv' ON CLIENT
-- USING delimiters ',';