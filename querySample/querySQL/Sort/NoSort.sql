{
  dimensions: [
    {
      column: {
        columnId: 'master',
        subColumnType: 0
      }
    }
  ],
};

-- COPY
WITH
  context AS (
    SELECT
      master
    FROM
      voyages
    GROUP BY
      master
  ),
  master_sort AS (
    SELECT
      DISTINCT master
    FROM
      voyages
  ),
  master_order AS (
    SELECT
      master,
      ROW_NUMBER() OVER () AS master_order
    FROM
      master_sort
  ),
  with_all AS (
    SELECT
      context.*,
      master_order.master_order
    FROM
      context
        INNER JOIN master_order ON context.master = master_order.master OR (context.master IS NULL AND master_order.master IS NULL)
  )
SELECT
  master,
  master_order
FROM
  with_all;
-- INTO './querySample/queryResults/Sort/NoSort.csv' ON CLIENT
-- USING delimiters ',';
