{
  dimensions: [
    {
      column: {
        columnId: 'master',
        subColumnType: 0
      },
      orderBy: {
        type: 'value',
        order: 'descending',
        value: {
          aggregationType: 'dst',
          column: {
            columnId: 'boatname',
            subColumnType: 0
          }
        }
      }
    },
    {
      column: {
        columnId: 'chamber',
        subColumnType: 0
      },
      orderBy: {
        type: 'value',
        order: 'descending',
        value: {
          aggregationType: 'dst',
          column: {
            columnId: 'boatname',
            subColumnType: 0
          }
        }
      }
    }
  ],
  values: [
    {
      aggregationType: 'dst',
      column: {
        columnId: 'boatname',
        subColumnType: 0
      }
    },
  ],
};

-- COPY
WITH
  context AS (
    SELECT
      COUNT(DISTINCT(boatname)) AS cnt_boat,
      master,
      chamber
    FROM
      voyages
    GROUP BY
      master,
      chamber
  ),
  master_sort AS (
    SELECT
      COUNT(DISTINCT(boatname)),
      master
    FROM
      voyages
    GROUP BY
      master
    ORDER BY
      1 DESC
  ),
  master_order AS (
    SELECT
      ROW_NUMBER() OVER () AS master_order,
      master
    FROM
      master_sort
  ),
  chamber_sort AS (
    SELECT
      COUNT(DISTINCT(boatname)),
      chamber
    FROM
      voyages
    GROUP BY
      chamber
    ORDER BY
      1 DESC
  ),
  chamber_order AS (
    SELECT
      ROW_NUMBER() OVER () AS chamber_order,
      chamber
    FROM
      chamber_sort
  ),
  with_all AS (
    SELECT
      context.*,
      master_order.master_order,
      chamber_order.chamber_order
    FROM
      context
        INNER JOIN master_order ON context.master = master_order.master OR (context.master IS NULL AND master_order.master IS NULL)
        INNER JOIN chamber_order ON context.chamber = chamber_order.chamber OR (context.chamber IS NULL AND chamber_order.chamber IS NULL)
  )
SELECT
  master,
  master_order,
  chamber,
  chamber_order,
  cnt_boat
FROM
  with_all;
-- INTO './querySample/queryResults/Sort/SortByValue.csv' ON CLIENT
-- USING delimiters ',';
