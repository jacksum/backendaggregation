{
  dimensions: [
    {
      column: {
        columnId: 'boatname',
        subColumnType: 0,
      },
      orderBy: {
        type: 'value',
        order: 'descending',
        value: {
          aggregationType: 'fst',
          column: {
            columnId: 'arrival_date',
            subColumnType: 0
          }
        }

      }
    }
  ]
};

-- reserved. this case can be implement as sort by value with FIRST method.
-- COPY
WITH
  context AS (
    SELECT
      boatname
    FROM
      voyages
    GROUP BY
      boatname 
  ),
  boatname_sort AS (
    SELECT
      DISTINCT boatname,
      sys.first_value(arrival_date) OVER ( --by column 肯定会用这个function
        PARTITION BY
          boatname
      ) AS first_of_arrival_date
    FROM
      voyages
    ORDER BY
      first_of_arrival_date DESC
  ),
  boatname_order AS (
    SELECT
      boatname,
      ROW_NUMBER() OVER () AS boatname_order
    FROM
      boatname_sort
  ),
  with_order AS (
    SELECT
      context.*,
      boatname_order.boatname_order
    FROM
      context
        INNER JOIN boatname_order ON context.boatname = boatname_order.boatname OR (context.boatname IS NULL AND boatname_order.boatname IS NULL)
  )
SELECT
  boatname,
  boatname_order
FROM
  with_order;
-- INTO './querySample/queryResults/Sort/SortByColumn.csv' ON CLIENT
-- USING delimiters ',';