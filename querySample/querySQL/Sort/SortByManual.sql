{
  dimensions: [
    {
      column: {
        columnId: 'master',
        subColumnType: 0,
      },
      orderBy: {
        type: 'manual',
        manual: ['ZON', 'MAAN', 'LEEUW']
      }
    }
  ],
};

-- COPY
With
  context AS (
    SELECT
      boatname
    FROM
      voyages
    GROUP BY
      boatname
  ),
  boatname_sort AS (
    SELECT
      boatname
    FROM
      voyages
    GROUP BY
      boatname
    ORDER BY (
      CASE boatname
        WHEN 'ZON'
        THEN 0
        WHEN 'MAAN'
        THEN 1
        WHEN 'LEEUW'
        THEN 2
        ELSE 3
      END
    ) ASC
 ),
  boatname_order AS (
    SELECT
      boatname,
      ROW_NUMBER() OVER () AS boatname_order
    FROM
      boatname_sort
 ),
  with_all AS (
    SELECT
      context.*,
      boatname_order.boatname_order
    FROM
      context
        INNER JOIN boatname_order ON context.boatname = boatname_order.boatname OR (context.boatname IS NULL AND boatname_order.boatname IS NULL)
  )
SELECT
  boatname,
  boatname_order
FROM
  with_all;
-- INTO './querySample/queryResults/Sort/SortByManual.csv' ON CLIENT
-- USING delimiters ',';