-- EXPLAIN
-- COPY
WITH
  data_view AS (   -- this is the step 2. filter 
    SELECT
      *
    FROM
      voyages
    WHERE
      arrival_date >= sys.str_to_date('1733-04-01T00:00:00', '%Y-%m-%dT%H:%M:%S')
      AND
      arrival_date < sys.str_to_date('1734-04-01T00:00:00', '%Y-%m-%dT%H:%M:%S')
  ),
  context AS ( ---this is the step 3
    SELECT
      EXTRACT(MONTH FROM arrival_date) AS arrival_date_month,--demission
      master,--demission
      COUNT(DISTINCT(type_of_boat)) AS dstcnt_of_type_of_boat,--value , value for calcFilter
      SUM(tonnage) AS sum_of_tonnage,--value for calcValue
      COUNT(*) AS cnt--value for calcValue
    FROM
      data_view
    GROUP BY
      master,
      arrival_date_month
  ),
-- filter
  with_partition_filter AS (---  this is step 6  roll-up value for partition rankcalcfilter 
    SELECT
      *,
      ROW_NUMBER() OVER (
        PARTITION BY
          arrival_date_month
        ORDER BY
          dstcnt_of_type_of_boat DESC
      ) AS rank_of_dstcnt_of_type_of_boat
    FROM
      context
  ),
  with_filter AS (--- 这个是calcfilter
    SELECT
      *
    FROM
      with_partition_filter
    WHERE
      rank_of_dstcnt_of_type_of_boat <= 4
  ),
-- sort
  arrival_date_month_sort AS (
    SELECT
      EXTRACT(MONTH FROM arrival_date) AS arrival_date_month
    FROM
      data_view
    GROUP BY
      arrival_date_month
    ORDER BY (
      CASE arrival_date_month
        WHEN 4
        THEN 1
        WHEN 5
        THEN 2
        WHEN 6
        THEN 3
        WHEN 7
        THEN 4
        WHEN 8
        THEN 5
        WHEN 9
        THEN 6
        WHEN 10
        THEN 7
        WHEN 11
        THEN 8
        WHEN 12
        THEN 9
        WHEN 1
        THEN 10
        WHEN 2
        THEN 11
        WHEN 3
        THEN 12
        ELSE 13
      END
    ) ASC
  ),
  arrival_date_month_order AS (
    SELECT
      *,
      ROW_NUMBER() OVER () AS arrival_date_month_order
    FROM
      arrival_date_month_sort
  ),
  master_sort AS (
    SELECT
      COUNT(DISTINCT(type_of_boat)) AS dstcnt_of_type_of_boat,
      master
    FROM
      data_view
    GROUP BY
      master
    ORDER BY
      1 DESC
  ),
  master_order AS (
    SELECT
      *,
      ROW_NUMBER() OVER () AS master_order
    FROM
      master_sort
  ),
-- calculate value
  with_order_filter AS (
    SELECT
      *
    FROM
      with_filter
        INNER JOIN arrival_date_month_order ON with_filter.arrival_date_month = arrival_date_month_order.arrival_date_month
        INNER JOIN master_order ON with_filter.master = master_order.master
    ORDER BY
      arrival_date_month_order ASC,
      master_order ASC
  ),
  with_all AS (
    SELECT
      *,
      CAST(SUM(sum_of_tonnage) OVER (---没partitionby , 但得添加order by 
        ORDER BY
          arrival_date_month_order ASC,
          master_order ASC
      ) AS DOUBLE) / SUM(cnt) OVER (
        ORDER BY
          arrival_date_month_order ASC,
          master_order ASC
      ) AS running_avg_on_avg_of_tonnage
    FROM
      with_order_filter
  )
SELECT
  arrival_date_month,
  arrival_date_month_order,
  master,
  master_order,
  dstcnt_of_type_of_boat,
  running_avg_on_avg_of_tonnage
FROM
  with_all;
-- INTO './querySample/queryResults/PivotSample.csv' ON CLIENT
-- USING delimiters ',';
