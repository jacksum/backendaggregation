-- COPY
SELECT
  master,
  type_of_boat
FROM
  voyages
WHERE
  type_of_boat IS NOT NULL
  AND
  master IS NOT NULL
ORDER BY
  master ASC,
  tonnage DESC
LIMIT
  200
OFFSET
  200;
-- INTO './querySample/queryResults/RawSample.csv' ON CLIENT
-- USING delimiters ',';