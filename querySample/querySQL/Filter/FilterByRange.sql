{
  dimensions: [
    {
      column: {
        columnId: 'arrival_date',
        subColumnType: 4
      }
    }
  ],
  values: [{
    aggregationType: 'sum',
    column: {
      columnId: 'tonnage',
      subColumnType: 0
    }
  }],
  filters: {
    type: 'and',
    items: [{
      columns: [{
        columnId: 'arrival_date',
        subColumnType: 0
      }],
      filterBy: {
        type: 20,
        exclude: false,
        range: {
          max: '1734-04-01T00:00:00',
          min: '1733-04-01T00:00:00',
          maxIncluded: false,
          minIncluded: true
        }
      }
    }]
  }
};

-- COPY
WITH
  context AS (
    SELECT
      EXTRACT(MONTH FROM arrival_date) AS arrival_date_month,
      SUM(tonnage) AS sum_of_tonnage
    FROM
      voyages
    WHERE
      arrival_date >= sys.str_to_date('1733-04-01T00:00:00', '%Y-%m-%dT%H:%M:%S')
      AND
      arrival_date < sys.str_to_date('1734-04-01T00:00:00', '%Y-%m-%dT%H:%M:%S')
    GROUP BY
      arrival_date_month
  )
SELECT
  arrival_date_month,
  sum_of_tonnage
FROM
  context
LIMIT 100;
-- INTO './querySample/queryResults/Filter/FilterByRange.csv' ON CLIENT
-- USING delimiters ',';
