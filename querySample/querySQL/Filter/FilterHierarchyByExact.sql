{
  dimensions: [
    {
      column: {
        columnId: 'master',
        subColumnType: 0
      }
    },
    {
      column: {
        columnId: 'boatname',
        subColumnType: 0
      }
    }
  ],
  values: [],
  filters: {
    type: 'and',
    items: [{
      columns: [{
        columnId: 'master',
        subColumnType: 0
      },{
        columnId: 'boatname',
        subColumnType: 0
      }],
      filterBy: {
        type: 10,
        range: {
          items: [{
            value: 'Alexander Simons',
            children: [{
              value: 'WALENBURG'
            }, {
              value: 'THEEBOOM'
            }]
          }, {
            value: 'Bruin Jansz.',
            children: [{
              value: 'BERKEL'
            }, {
              value: 'SPAARNWOUDE'
            }]
          }]
        },
        exclude: false
      }
    }]
  },
};

-- COPY
WITH
  context AS (
    SELECT
      master,
      boatname
    FROM
      voyages
    WHERE (
      master = 'Alexander Simons' AND boatname = 'WALENBURG'
      OR
      master = 'Alexander Simons' AND boatname = 'THEEBOOM'
      OR
      master = 'Bruin Jansz.' AND boatname = 'BERKEL'
      OR
      master = 'Bruin Jansz.' AND boatname = 'SPAARNWOUDE'
    )
    GROUP BY
      master,
      boatname
  )
SELECT
  master,
  boatname
FROM
  context
LIMIT 100;
-- INTO './querySample/queryResults/Filter/FilterHierarchyByExact.csv' ON CLIENT
-- USING delimiters ',';
