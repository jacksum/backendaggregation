{
  dimensions: [
    {
      column: {
        columnId: 'master',
        subColumnType: 0
      }
    }
  ],
  filters: {
    type: 'and',
    items: [{
      columns: [{
        columnId: 'master',
        subColumnType: 0
      }],
      filterBy: {
        type: 30,
        range: {
          pattern: 'Alex',
          caseSensitive: true,
        },
        exclude: false
      }
    }]
  },
};

-- COPY
WITH
  context AS (
    SELECT
      master
    FROM
      voyages
    WHERE
      master LIKE 'Alex%'
    GROUP BY
      master
  )
SELECT
  master
FROM
  context
LIMIT 100; --忽略
-- INTO './querySample/queryResults/Filter/FilterByWildcard.csv' ON CLIENT
-- USING delimiters ',';
