{
  dimensions: [
    {
      column: {
        columnId: 'master',
        subColumnType: 0
      }
    }
  ],
  filters: {
    type: 'and',
    items: [{
      columns: [{
        columnId: 'master',
        subColumnType: 0
      }],
      filterBy: {
        type: 10,
        range: {
          items: [{
            value: 'Alexander Simons',
          }, {
            value: 'Bruin Jansz.',
          }]
        },
        exclude: false
      }
    }]
  },
};

-- COPY
WITH
  context AS (
    SELECT
      master
    FROM
      voyages
    WHERE
      master IN ('Alexander Simons', 'Bruin Jansz.')
    GROUP BY
      master
  )
SELECT
  master
FROM
  context
LIMIT 100;
-- INTO './querySample/queryResults/Filter/FilterByExact.csv' ON CLIENT
-- USING delimiters ',';

