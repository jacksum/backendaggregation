-- COPY
WITH
  context AS (
    SELECT
      type_of_boat,
      master,
      SUM(tonnage) AS sum_of_tonnage
    FROM
      voyages
    WHERE
      type_of_boat IS NOT NULL
      AND
      tonnage IS NOT NULL
      AND
      master IN (
        'Alexander Simons',
        'Bruin Jansz.',
        'Diedlof Kroes',
        'Egbert Kalf',
        'Gerrit Koek'
      )
    GROUP BY
      type_of_boat,
      master
  ),
  with_rank AS (
    SELECT
      *,
      ROW_NUMBER() OVER (
        PARTITION BY
          master
        ORDER BY
          sum_of_tonnage DESC
      ) AS rank_of_sum_of_tonnage
    FROM
      context
  )
SELECT
  type_of_boat,
  master
FROM
  with_rank
WHERE
  rank_of_sum_of_tonnage <= 2;
-- INTO './querySample/queryResults/CalcFilter/PartitionRankFilter.csv' ON CLIENT
-- USING delimiters ',';