-- COPY
WITH
  context AS (
    SELECT
      type_of_boat,
      master
    FROM
      voyages
    WHERE
      type_of_boat IS NOT NULL
      AND
      tonnage IS NOT NULL
      AND
      master IN (
        'Alexander Simons',
        'Bruin Jansz.',
        'Diedlof Kroes',
        'Egbert Kalf',
        'Gerrit Koek'
      )
    GROUP BY
      type_of_boat,
      master
  ),
  master_sort AS (
    SELECT
      SUM(tonnage) AS sum_of_tonnage,
      master
    FROM
      voyages
    WHERE
      tonnage IS NOT NULL
      AND
      master IN (
        'Alexander Simons',
        'Bruin Jansz.',
        'Diedlof Kroes',
        'Egbert Kalf',
        'Gerrit Koek'
      )
    GROUP BY
      master
  ),
  master_filter AS (
    SELECT
      CAST(ROW_NUMBER() OVER (
        ORDER BY
          sum_of_tonnage DESC
      ) - 1 AS DOUBLE) / (COUNT(*) OVER () - 1) AS percent_rank,
      master
    FROM
      master_sort
  )
SELECT
  type_of_boat,
  master
FROM
  context
WHERE
  master IN (
    SELECT
      master
    FROM
      master_filter
    WHERE
      percent_rank <= 0.4
  );
-- INTO './querySample/queryResults/CalcFilter/GroupRankFilterInPercentage.csv' ON CLIENT
-- USING delimiters ',';