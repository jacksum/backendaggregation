-- COPY
WITH
  context AS (
    SELECT
      type_of_boat,
      master
    FROM
      voyages
    WHERE
      type_of_boat IS NOT NULL
      AND
      master IN (
        'Alexander Simons',
        'Bruin Jansz.',
        'Diedlof Kroes',
        'Egbert Kalf',
        'Gerrit Koek'
      )
    GROUP BY
      type_of_boat,
      master
  ),
  master_filter AS (
    SELECT
      master,
      SUM(tonnage) AS sum_of_tonnage
    FROM
      voyages
    WHERE
      master IN (
        'Alexander Simons',
        'Bruin Jansz.',
        'Diedlof Kroes',
        'Egbert Kalf',
        'Gerrit Koek'
      )
    GROUP BY
      master
  )
SELECT
  type_of_boat,
  master
FROM
  context
WHERE
  master IN (
    SELECT
      master
    FROM
      master_filter
    WHERE
      sum_of_tonnage > 2000
  );
-- INTO './querySample/queryResults/CalcFilter/GroupIdentityFilter.csv' ON CLIENT
-- USING delimiters ',';