-- COPY
WITH
  context AS (
    SELECT
      type_of_boat,
      master,
      SUM(tonnage) AS sum_of_tonnage
    FROM
      voyages
    WHERE
      type_of_boat IS NOT NULL
      AND
      master IN (
        'Alexander Simons',
        'Bruin Jansz.',
        'Diedlof Kroes',
        'Egbert Kalf',
        'Gerrit Koek'
      )
    GROUP BY
      type_of_boat,
      master
  )
SELECT
  type_of_boat,
  master
FROM
  context
WHERE
  sum_of_tonnage > 2000;
-- INTO './querySample/queryResults/CalcFilter/PartitionIdentityFilter.csv' ON CLIENT
-- USING delimiters ',';