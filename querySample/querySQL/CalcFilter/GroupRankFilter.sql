COPY
WITH
  data_view AS (
    SELECT
      *
    FROM
      voyages
    WHERE
      type_of_boat IS NOT NULL
      AND
      tonnage IS NOT NULL
      AND
      master IN (
        'Alexander Simons',
        'Bruin Jansz.',
        'Diedlof Kroes',
        'Egbert Kalf',
        'Gerrit Koek'
      )
  ),
  context AS (
    SELECT
      type_of_boat,
      master
    FROM
      data_view
    GROUP BY
      type_of_boat,
      master
  ),
  sum_of_tonnage_on_master AS (
    SELECT
      SUM(tonnage) AS sum_of_tonnage,
      master
    FROM
      data_view
    GROUP BY
      master
  ),
  master_filter AS (
    SELECT
      ROW_NUMBER() OVER (
        ORDER BY
          sum_of_tonnage DESC
      ) AS rank_of_sum_of_tonnage,
      master
    FROM
      sum_of_tonnage_on_master
  ),
  with_all AS (
    SELECT
      context.*
    FROM
      context
        INNER JOIN master_filter ON context.master = master_filter.master OR (context.master IS NULL AND master_filter.master IS NULL)
    WHERE
      rank_of_sum_of_tonnage <= 2
  )
SELECT
  type_of_boat,
  master
FROM
  with_all
INTO './querySample/queryResults/CalcFilter/GroupRankFilter.csv' ON CLIENT
USING delimiters ',';