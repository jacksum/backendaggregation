{
  dimensions: [{
    column: {
      columnId: 'master',
      subColumnType: 0
    }
  },{
    column: {
      columnId: 'type_of_boat',
      subColumnType: 0
    }
  }],
  values: [{
    aggregationType: 'fst',
    column: {
      columnId: 'boatname',
      subColumnType: 0
    }
  }]
};

-- COPY
WITH
  context AS (
    SELECT
      master,
      type_of_boat
    FROM
      voyages
    GROUP BY
      master,
      type_of_boat
  ),
  master_sort AS (
    SELECT
      DISTINCT master
    FROM
      context
  ),
  master_order AS (
    SELECT
      master,
      ROW_NUMBER() OVER () AS master_order
    FROM
      master_sort
  ),
  type_of_boat_sort AS (
    SELECT
      DISTINCT type_of_boat
    FROM
      context
  ),
  type_of_boat_order AS (
    SELECT
      type_of_boat,
      ROW_NUMBER() OVER () AS type_of_boat_order
    FROM
      type_of_boat_sort
  ),
  boatname_fst AS (
    SELECT
      DISTINCT master,
      type_of_boat,
      sys.first_value(boatname) OVER (
        PARTITION BY
          master,
          type_of_boat
      ) AS first_of_boatname
    FROM
      voyages
  ),
  with_all AS (
    SELECT
      context.*,
      boatname_fst.first_of_boatname,
      master_order.master_order,
      type_of_boat_order.type_of_boat_order
    FROM
      context
        INNER JOIN boatname_fst ON (context.master = boatname_fst.master OR (context.master IS NULL AND boatname_fst.master IS NULL)) AND (context.type_of_boat = boatname_fst.type_of_boat OR (context.type_of_boat IS NULL AND boatname_fst.type_of_boat IS NULL))
        INNER JOIN master_order ON (context.master = master_order.master OR (context.master IS NULL AND master_order.master IS NULL))
        INNER JOIN type_of_boat_order ON (context.type_of_boat = type_of_boat_order.type_of_boat OR (context.type_of_boat IS NULL AND type_of_boat_order.type_of_boat IS NULL))
  )
SELECT
  master,
  master_order,
  type_of_boat,
  type_of_boat_order,
  first_of_boatname
FROM
  with_all;
-- INTO './querySample/queryResults/First.csv' ON CLIENT
-- USING delimiters ',';